<?php

use App\Model\Author;
use App\Model\Category;
use App\Model\Post;
use Framework\App;
use Framework\Database\OrderedColumn;
use Framework\Database\QueryBuilderOperator as QBO;

require __DIR__ . '/bootstrap.php';

$app = App::GetInstance();
$app->configureFrom(__DIR__ . '/../config_dev.json');
$db = $app->getDatabaseConnections()['default'];
$pdo = $db->getPdo();

$base = 'https://reflection.to';
$xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

$xml .= makeUrlTag($base);
//$xml .= makeUrlTag("{$base}/privacy");
//$xml .= makeUrlTag("{$base}/terms");

$category_builder = Post::GetSelectQueryBuilder('t')
    ->where('t.category_id', QBO::EQUAL, ':category_id')
    ->order('t.date_updated', OrderedColumn::DESC)
    ->limit(1);
$category_statement = $pdo->prepare($category_builder->getQuery());
$category_statement->setFetchMode(PDO::FETCH_CLASS, Post::class);
foreach (Category::ReadAll($pdo) as $category) {
    $url = "{$base}{$category->getUrl()}";
    $xml .= makeUrlTag($url);

    $list_url = "{$base}{$category->getPostsListUrl()}";
    $category_statement->execute([':category_id' => $category->getId()]);
    /** @var Post $recent_category_post */
    $recent_category_post = $category_statement->fetch();
    $xml .= makeUrlTag($list_url, $recent_category_post->getDateUpdated(),
        'weekly', 0.9);
}

$author_builder = Post::GetSelectQueryBuilder('t')
    ->where('t.author_id', QBO::EQUAL, ':author_id')
    ->order('t.date', OrderedColumn::DESC)
    ->limit(1);
$author_statement = $pdo->prepare($author_builder->getQuery());
$author_statement->setFetchMode(PDO::FETCH_CLASS, Post::class);
foreach (Author::ReadAll($pdo) as $author) {
    $url = "{$base}{$author->getUrl()}";
    $author_statement->execute([':author_id' => $author->getId()]);
    /** @var Post $recent_author_post */
    $recent_author_post = $author_statement->fetch();
    $xml .= makeUrlTag($url, $recent_author_post->getDate(),
        'weekly', 0.8);
}

foreach (Post::ReadAll($pdo) as $post) {
    $url = "{$base}{$post->getUrl()}";
    $xml .= makeUrlTag($url, $post->getDateUpdated(), null, 1.0);
}

$xml .= "</urlset>";

$output_file = $argv[1] ?? __DIR__ . '/../www/sitemap.xml';
$ping_search_engines = $arvg[2] ?? true;
$output_dir = dirname($output_file);
if (false === file_exists($output_dir))
    mkdir($output_dir, 0755, true);
file_put_contents($output_file, $xml);
chmod($output_file, 0644);

if (true === $ping_search_engines) {
    $encoded_sitemap_url = urlencode("{$base}/sitemap.xml");
    foreach (["https://google.com", "https://bing.com"] as $search_engine_base_url) {
        $ping_url = "{$search_engine_base_url}/ping?sitemap={$encoded_sitemap_url}";
        $ch = curl_init($ping_url);
        try {
            curl_exec($ch);
        } catch (Exception $exception) {
            continue;
        } finally {
            curl_close($ch);
        }
    }
}

/**
 * @param string $url
 * @param DateTimeInterface|null $last_mod
 * @param string|null $change_freq
 * @param float|null $priority
 * @return string
 */
function makeUrlTag(string $url,
    ?DateTimeInterface $last_mod = null,
    ?string $change_freq = 'monthly',
    ?float $priority = 0.5): string
{
    $url = htmlspecialchars($url, ENT_QUOTES | ENT_HTML5, 'UTF-8');

    $xml = '<url>';
    $xml .= "<loc>{$url}</loc>";
    if ($last_mod)
        $xml .= "<lastmod>{$last_mod->format(DATE_W3C)}</lastmod>";
    if ($change_freq)
        $xml .= "<changefreq>{$change_freq}</changefreq>";
    if ($priority)
        $xml .= "<priority>{$priority}</priority>";
    $xml .= '</url>';

    return $xml;
}