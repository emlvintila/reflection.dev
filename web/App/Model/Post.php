<?php

namespace App\Model;

use App\Parsedown\Parsedown;
use DateTimeImmutable;
use Exception;
use Framework\Database\QueryBuilderOperator as QBO;
use Framework\Database\QueryBuilderSelect;
use Framework\Database\TableJoin;
use Framework\Model\Entity;
use Framework\URL;
use PDO;

class Post extends Entity
{
    /** @var string */
    protected $title = '';
    /** @var string */
    protected $body = '';
    /** @var DateTimeImmutable */
    protected $date;
    /** @var DateTimeImmutable */
    protected $date_updated;
    /** @var int */
    protected $category_id;
    /** @var int */
    protected $author_id;
    /** @var int */
    protected $like_count;
    /** @var Category */
    protected $category;
    /** @var Author */
    protected $author;
    /** @var string */
    protected $title_metaphone;

    public function __construct()
    {
        try {
            $this->date = new DateTimeImmutable($this->date);
        } catch (Exception $e) {
            $this->date = new DateTimeImmutable();
        }
        try {
            $this->date_updated = new DateTimeImmutable($this->date_updated);
        } catch (Exception $e) {
            $this->date_updated = new DateTimeImmutable();
        }
        $this->category_id = (int)$this->category_id;
    }

    public static function GetSelectQueryBuilder(string $alias = null): QueryBuilderSelect
    {
        $l = $alias ?? static::GetTableAlias();
        $r = $l . 'lj';
        $builder = QueryBuilderSelect::Create(static::GetTableName(), $l)
            ->join("reflection.posts_likes {$r}", TableJoin::LEFT, "{$l}.id", QBO::EQUAL, "{$r}.post_id")
            ->group("{$l}.id")
            ->select(["{$l}.*", 'like_count' => "COUNT({$r}.id)"]);

        return $builder;
    }

    /** @return string */
    protected static function GetTableName(): string
    {
        return 'reflection.posts';
    }

    /** @return string[] */
    public function getModelStateErrors(): array
    {
        $errors = [];

        static $title_pattern = '/[\w\d \-]+/';
        if (0 === preg_match($title_pattern, $this->title))
            $errors[] = "Invalid title (must match $title_pattern).";

        return $errors;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->title,
            $this->body,
            $this->date,
            $this->date_updated,
            $this->category_id,
            $this->author_id
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->title,
            $this->body,
            $this->date,
            $this->date_updated,
            $this->category_id,
            $this->author_id
            ) = unserialize($serialized);
    }

    /**
     * @param string|null $user_id
     * @return bool|null
     */
    public function isLikedBy(?string $user_id): ?bool
    {
        if (null === $user_id)
            return false;

        $builder = QueryBuilderSelect::Create('reflection.posts_likes', 'l')
            ->select(['COUNT(*)'])
            ->where('l.user_id', '=', ':user_id')
            ->where('l.post_id', '=', ':post_id');
        $statement = $this->pdo->prepare($builder->getQuery());
        $statement->execute([
            ':user_id' => $user_id,
            ':post_id' => $this->getId()
        ]);

        return (bool)$statement->fetch(PDO::FETCH_NUM)[0];
    }

    /** @return string */
    public function getTitleMetaphone(): string
    {
        return $this->title_metaphone;
    }

    /** @return string */
    public function getJsonLd()
    {
        $category = $this->getCategory();
        $author = $this->getAuthor();

        $title = $this->getTitle();
        $category_name = $category->getFriendlyName();
        $url = URL::FromRoot($this->getUrl());
        $date = $this->getDate()->format(DATE_ISO8601);
        $word_count = str_word_count($this->getBody());
        $image = URL::FromRoot($category->getSplashImage());
        $author_name = $author->getName();
        $author_url = URL::FromRoot($author->getUrl());
        $like_count = $this->getLikeCount();

        return /** @lang JSON */ <<<JSONLD
{
    "@context": "http://schema.org",
    "@type": "Article",
    "name": "{$title}",
    "headline": "{$title}",
    "articleSection": "{$category_name}",
    "url": "{$url}",
    "datePublished": "{$date}",
    "dateModified": "{$date}",
    "wordCount": "{$word_count}",
    "image": "{$image}",
    "author": {
        "@type": "Person",
        "name": "{$author_name}",
        "url": "{$author_url}"
    },
    "publisher": {
        "@type": "Person",
        "name": "{$author_name}",
        "url": "{$author_url}"
    },
    "interactionStatistic": [
        {
            "@type": "InteractionCounter",
            "interactionType": "http://schema.org/LikeAction",
            "userInteractionCount": "{$like_count}"
        }
    ],
    "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "{$url}"
    }
}
JSONLD;
    }

    /** @return Category */
    public function getCategory(): Category
    {
        if (null === $this->category)
            $this->category = Category::ReadSingle($this->pdo, $this->category_id);

        return $this->category;
    }

    /** @return Author */
    public function getAuthor(): Author
    {
        if (null === $this->author)
            $this->author = Author::ReadSingle($this->pdo, $this->author_id);

        return $this->author;
    }

    /** @return string */
    public function getTitle(): string
    {
        return $this->title;
    }

    /** @param string $title */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getUrl()
    {
        $language = $this->getCategory();
        $uri_title = preg_replace('/[\s-]+/', '_', $this->title);

        return "/posts/{$language->getUriIdentifier()}/{$this->id}-{$uri_title}";
    }

    /** @return DateTimeImmutable */
    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    /** @return string */
    public function getBody(): string
    {
        return $this->body;
    }

    /** @param string $body */
    public function setBody(string $body): void
    {
        $this->body = $body;
    }

    /** @return int */
    public function getLikeCount(): int
    {
        return $this->like_count;
    }

    /** @return DateTimeImmutable */
    public function getDateUpdated(): DateTimeImmutable
    {
        return $this->date_updated;
    }

    public function getFirstParagraph()
    {
        $matches = [];
        if (1 === preg_match('/^(.*?)(?:(?:\r\n)|\r|\n){2}/',
                $this->getBody(), $matches))
            $paragraph = Parsedown::instance()->text($matches[1]);
        else
            $paragraph = $this->getBody();

        return strip_tags($paragraph);
    }

    /** @return array */
    protected function getInsertUpdateColumns(): array
    {
        return [
            'title' => $this->getTitle(),
            'body' => $this->getBody(),
            'category_id' => $this->getCategoryId(),
            'author_id' => $this->getAuthorId()
        ];
    }

    /** @return int */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /** @param int $category_id */
    public function setCategoryId(int $category_id): void
    {
        $this->category_id = $category_id;
    }

    /** @return int */
    public function getAuthorId(): int
    {
        return $this->author_id;
    }

    /** @param int $author_id */
    public function setAuthorId(int $author_id): void
    {
        $this->author_id = $author_id;
    }
}
