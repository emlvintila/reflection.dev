<?php

namespace App\Model;

use Framework\Model\Entity;
use PDO;

class Category extends Entity
{
    const URI_ID_PATTERN = '/[\w\d_\-]+/';
    /** @var string */
    protected $uri_identifier;
    /** @var string */
    protected $friendly_name;
    /** @var string */
    protected $icon_image;
    /** @var string */
    protected $splash_image;

    /**
     * @param PDO $pdo
     * @param string $uri_id
     * @return Category|null
     */
    public static function ReadFromDatabaseByUriId(PDO $pdo, string $uri_id): ?self
    {
        $statement = $pdo->prepare("SELECT * FROM reflection.categories WHERE uri_identifier = :uri_id LIMIT 1");
        if (false === $statement)
            return null;

        $statement->bindValue(':uri_id', $uri_id);
        if (false === $statement->execute())
            return null;

        $statement->setFetchMode(PDO::FETCH_CLASS, Category::class);
        /** @var Category $obj */
        $obj = $statement->fetch();
        if (!$obj)
            return null;

        $obj->setPdo($pdo);

        return $obj;
    }

    /** @return string */
    protected static function GetTableName(): string
    {
        return 'reflection.categories';
    }

    public function getUnsubscribeUrl()
    {
        return "/categories/{$this->getUriIdentifier()}/unsubscribe";
    }

    /** @return string */
    public function getFriendlyName(): string
    {
        return $this->friendly_name;
    }

    /** @param string $friendly_name */
    public function setFriendlyName(string $friendly_name): void
    {
        $this->friendly_name = $friendly_name;
    }

    /** @return string */
    public function getIconImage(): string
    {
        return $this->icon_image;
    }

    /** @param string $icon_image */
    public function setIconImage(string $icon_image): void
    {
        $this->icon_image = $icon_image;
    }

    /** @return string */
    public function getSplashImage(): string
    {
        return $this->splash_image;
    }

    /** @param string $splash_image */
    public function setSplashImage(string $splash_image): void
    {
        $this->splash_image = $splash_image;
    }

    /** @return string[] */
    public function getModelStateErrors(): array
    {
        $errors = [];

        if (false === preg_match(static::URI_ID_PATTERN, $this->uri_identifier))
            $errors[] = "Invalid URI Identifier (must match " . static::URI_ID_PATTERN . ").";

        return $errors;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->uri_identifier,
            $this->friendly_name
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->uri_identifier,
            $this->friendly_name
            ) = unserialize($serialized);
    }

    public function getUrl()
    {
        return "/categories/{$this->getUriIdentifier()}";
    }

    public function getPostsListUrl()
    {
        return "{$this->getUrl()}/posts";
    }

    public function getSubscribeUrl()
    {
        return "{$this->getUrl()}/subscribe";
    }

    /** @return string */
    public function getUriIdentifier(): string
    {
        return $this->uri_identifier;
    }

    /** @param string $uri_identifier */
    public function setUriIdentifier(string $uri_identifier): void
    {
        $this->uri_identifier = $uri_identifier;
    }

    /** @return array */
    protected function getInsertUpdateColumns(): array
    {
        return [
            'uri_identifier' => $this->getUriIdentifier(),
            'friendly_name' => $this->getFriendlyName(),
            'icon_image' => $this->getIconImage(),
            'splash_image' => $this->getSplashImage()
        ];
    }
}
