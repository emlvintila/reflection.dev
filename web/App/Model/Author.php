<?php
namespace App\Model;

use App\RegexConst;
use Framework\Model\Entity;
use Framework\URL;
use L;
use PDO;

class Author extends Entity
{
    /** @var string */
    protected $name;
    /** @var string */
    protected $email;
    /** @var string */
    protected $password_hash;
    /** @var string|null */
    protected $instagram_username;
    /** @var string|null */
    protected $facebook_username;
    /** @var string|null */
    protected $github_username;
    /** @var string|null */
    protected $twitter_username;

    /**
     * @param PDO $pdo
     * @param string $name
     * @return Author
     */
    public static function ReadSingleByName(PDO $pdo, string $name): Author
    {
        return static::ReadSingleBy($pdo, ['name' => $name]);
    }

    /** @return string */
    protected static function GetTableName(): string
    {
        return 'reflection.authors';
    }

    /** @return string */
    public function getName(): string
    {
        return $this->name;
    }

    /** @param string $name */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /** @return string */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /** @return string */
    public function getPasswordHash(): string
    {
        return $this->password_hash;
    }

    /** @param string $password_hash */
    public function setPasswordHash(string $password_hash): void
    {
        $this->password_hash = $password_hash;
    }

    /** @return string|null */
    public function getInstagramUsername(): ?string
    {
        return $this->instagram_username;
    }

    /** @param string|null $instagram_username */
    public function setInstagramUsername(?string $instagram_username): void
    {
        $this->instagram_username = $instagram_username;
    }

    /** @return string|null */
    public function getFacebookUsername(): ?string
    {
        return $this->facebook_username;
    }

    /** @param string|null $facebook_username */
    public function setFacebookUsername(?string $facebook_username): void
    {
        $this->facebook_username = $facebook_username;
    }

    /** @return string|null */
    public function getGithubUsername(): ?string
    {
        return $this->github_username;
    }

    /** @param string|null $github_username */
    public function setGithubUsername(?string $github_username): void
    {
        $this->github_username = $github_username;
    }

    /** @return string|null */
    public function getTwitterUsername(): ?string
    {
        return $this->twitter_username;
    }

    /** @param string|null $twitter_username */
    public function setTwitterUsername(?string $twitter_username): void
    {
        $this->twitter_username = $twitter_username;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->getId(),
            $this->getName(),
            $this->getEmail(),
            $this->getPasswordHash(),
            $this->getInstagramUsername(),
            $this->getFacebookUsername(),
            $this->getTwitterUsername()
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->name,
            $this->email,
            $this->password_hash,
            $this->instagram_username,
            $this->facebook_username,
            $this->github_username,
            $this->twitter_username
            ) = unserialize($serialized);
    }

    /** @return string[] */
    public function getModelStateErrors(): array
    {
        // TODO: Implement getModelStateErrors() method.
        $errors = [];
        if (0 === preg_match(RegexConst::EMAIL, $this->getEmail()))
            $errors[] = L::errors_model_email;

        return $errors;
    }

    public function getUrl()
    {
        return "/author/{$this->name}";
    }

    /** @return array */
    protected function getInsertUpdateColumns(): array
    {
        return [
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'password_hash' => $this->getPasswordHash(),
            'instagram_username' => $this->getInstagramUsername(),
            'facebook_username' => $this->getFacebookUsername(),
            'github_username' => $this->getGithubUsername(),
            'twitter_username' => $this->getTwitterUsername()
        ];
    }

    /** @return string */
    public function getJsonLd()
    {
        $name = $this->getName();
        $url = URL::FromRoot($this->getUrl());

        return /** @lang JSON */ <<<JSONLD
{
    "@context": "http://schema.org",
    "@type": "Person",
    "name": "{$name}",
    "url": "{$url}"
}
JSONLD;
    }
}
