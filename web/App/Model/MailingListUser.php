<?php

namespace App\Model;

use App\RegexConst;
use Framework\Model\Entity;
use L;

class MailingListUser extends Entity
{
    /** @var string */
    protected $name;
    /** @var string */
    protected $email;
    /** @var int */
    protected $category_id;

    /** @return string */
    public function getName(): string
    {
        return $this->name;
    }

    /** @param string $name */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /** @return string */
    public function getEmail(): string
    {
        return $this->email;
    }

    /** @param string $email */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /** @return int */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /** @param int $category_id */
    public function setCategoryId(int $category_id): void
    {
        $this->category_id = $category_id;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->getId(),
            $this->getName(),
            $this->getEmail(),
            $this->getCategoryId()
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->name,
            $this->email,
            $this->category_id
            ) = unserialize($serialized);
    }

    /** @return string */
    protected static function GetTableName(): string
    {
        return 'reflection.mailing_list_subscriptions';
    }

    /** @return array */
    protected function getInsertUpdateColumns(): array
    {
        return [
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'category_id' => $this->getCategoryId()
        ];
    }

    /** @return string[] */
    public function getModelStateErrors(): array
    {
        $errors = [];
        if (0 === preg_match(RegexConst::EMAIL, $this->getEmail()))
            $errors[] = L::errors_model_email;

        return $errors;
    }
}
