<?php

namespace App\OAuth;

use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;

class FacebookHelper
{
    /** @var string */
    public const APP_ID = '652902528490468';
    /** @var string */
    public const APP_SECRET = '5afec63b870ec0a893581b624cddc9b0';
    /** @var Facebook */
    protected static $fb;

    /**
     * @return Facebook
     * @throws FacebookSDKException
     */
    public static function GetFacebook(): Facebook
    {
        if (static::$fb === null)
            static::$fb = new Facebook([
                'app_id' => FacebookHelper::APP_ID,
                'app_secret' => FacebookHelper::APP_SECRET,
                'default_graph_version' => 'v3.3',
            ]);

        return static::$fb;
    }
}
