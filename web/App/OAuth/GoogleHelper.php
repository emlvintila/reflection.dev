<?php

namespace App\OAuth;

use Google_Client;
use Google_Exception;

class GoogleHelper
{
    /** @var Google_Client */
    protected static $client;

    /**
     * @return Google_Client
     * @throws Google_Exception
     */
    public static function GetClient(): Google_Client
    {
        if (null === static::$client) {
            $client = new Google_Client();
            $client->setAuthConfig($_SERVER['DOCUMENT_ROOT'] . '/../client_secrets_google.json');
            $client->setAccessType("offline");
            $client->setIncludeGrantedScopes(true);
            static::$client = $client;
        }

        return static::$client;
    }
}
