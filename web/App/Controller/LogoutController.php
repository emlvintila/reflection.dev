<?php

namespace App\Controller;

use Framework\Controller\Controller;
use Framework\Controller\RedirectResponse;

class LogoutController extends Controller
{
    public function __construct()
    {
    }

    /** @return RedirectResponse */
    public function logoutAction(): RedirectResponse
    {
        session_destroy();

        return $this->redirectTo('/');
    }
}
