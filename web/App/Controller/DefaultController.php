<?php

namespace App\Controller;

use Framework\Controller\Controller;
use Framework\Controller\RenderResponse;

class DefaultController extends Controller
{
    public function __construct()
    {
    }

    /** @return RenderResponse */
    public function notFoundAction(): RenderResponse
    {
        return $this->renderHttpStatusPage(404);
    }
}
