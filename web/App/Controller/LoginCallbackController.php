<?php

namespace App\Controller;

use App\OAuth\FacebookHelper;
use App\OAuth\GoogleHelper;
use App\OAuth\TwitterHelper;
use App\OAuthSession;
use Exception;
use Facebook\Exceptions\FacebookSDKException;
use Framework\Controller\Controller;
use Framework\Controller\RedirectResponse;
use Framework\Controller\RenderResponse;
use Framework\Controller\Response;
use Google_Exception;
use Google_Service_Oauth2;
use TwitterAPIExchange;

class LoginCallbackController extends Controller
{
    /**
     * LoginCallbackController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return RedirectResponse|RenderResponse
     * @throws FacebookSDKException
     * @throws Exception
     */
    public function getFbCallbackAction(): Response
    {
        $fb = FacebookHelper::GetFacebook();
        $helper = $fb->getRedirectLoginHelper();
        $accessToken = $helper->getAccessToken();

        if (!isset($accessToken) || null === $accessToken) {
            if ($helper->getError()) {
                // $helper->getError(),
                // $helper->getErrorCode(),
                // $helper->getErrorReason(),
                // $helper->getErrorDescription()
                throw new Exception($helper->getError(), $helper->getErrorCode());
                // return $this->renderHttpStatusPage(401);
            } else {
                return $this->renderHttpStatusPage(400);
            }
        }

        $oAuth2Client = $fb->getOAuth2Client();

        if (!$accessToken->isLongLived()) {
            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken)->getValue();
        }

        $response = $fb->get('/me?fields=id,name', $accessToken);
        $user = $response->getGraphUser();
        $user_id = $user->getId();
        $user_name = $user->getName();

        $session = OAuthSession::GetInstance();
        $session->setOAuthProvider(OAuthSession::AUTH_PROVIDER_FACEBOOK);
        $session->setOAuthToken($accessToken);
        $session->setOAuthTokenSecret(null);
        $session->setOAuthUserId($user_id);
        $session->setOAuthUserName($user_name);

        return $this->redirectTo('/');
    }

    /**
     * @return RedirectResponse|RenderResponse
     * @throws Exception
     */
    public function getTwitterCallbackAction(): Response
    {
        $oauth_token = $_GET['oauth_token'] ?? null;
        $oauth_verifier = $_GET['oauth_verifier'] ?? null;
        if (!$oauth_token || !$oauth_verifier || $oauth_token !== OAuthSession::GetInstance()->getOAuthToken())
            return $this->renderHttpStatusPage(400);

        $tw = new TwitterAPIExchange(TwitterHelper::GetSettings());
        $response = $tw->buildOauth('https://api.twitter.com/oauth/access_token', 'POST')
            ->setPostfields([
                'oauth_token' => $oauth_token,
                'oauth_verifier' => $oauth_verifier
            ])
            ->performRequest(true, [
                CURLOPT_SSL_VERIFYPEER => false
            ]);
        if ($tw->getHttpStatusCode() !== 200) {
            throw new Exception('Twitter returned non-200 status');
        }

        list(
            $oauth_token,
            $oauth_token_secret,
            $user_id,
            $user_name
            ) = array_map(function ($s) {
            return explode('=', $s, 2)[1];
        }, explode('&', $response));

        $session = OAuthSession::GetInstance();
        $session->setOAuthProvider(OAuthSession::AUTH_PROVIDER_TWITTER);
        $session->setOAuthToken($oauth_token);
        $session->setOAuthTokenSecret($oauth_token_secret);
        $session->setOAuthUserId($user_id);
        $session->setOAuthUserName($user_name);

        return $this->redirectTo('/');
    }

    /**
     * @return RedirectResponse|RenderResponse
     * @throws Google_Exception
     */
    public function getGoogleCallbackAction()
    {
        $code = $_GET['code'] ?? null;
        $error = $_GET['error'] ?? null;
        if ($error)
            return $this->renderHttpStatusPage(503);
        if (!$code)
            return $this->renderHttpStatusPage(400);

        $google = GoogleHelper::GetClient();

        $google->fetchAccessTokenWithAuthCode($code);
        $oauth_token = $google->getAccessToken();
        $service = new Google_Service_Oauth2($google);
        $user = $service->userinfo->get();

        $session = OAuthSession::GetInstance();
        $session->setOAuthProvider(OAuthSession::AUTH_PROVIDER_GOOGLE);
        $session->setOAuthToken($oauth_token['access_token']);
        $session->setOAuthUserId($user->getId());
        $session->setOAuthUserName($user->getGivenName());

        return $this->redirectTo('/');
    }
}
