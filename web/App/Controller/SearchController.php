<?php

namespace App\Controller;

use App\Logging\MixedLogger;
use App\Model\Post;
use App\ViewModel\SearchViewModel;
use Framework\Controller\Controller;
use Framework\DatabaseConnection;
use Framework\View\View;
use PDO;

class SearchController extends Controller
{
    protected const MAX_LEVENSHTEIN_DISTANCE = 3;
    protected const MIN_WORD_LENGTH = 2;

    protected const MAX_RESULTS = 25;
    /** @var DatabaseConnection */
    protected $connection;
    /** @var MixedLogger */
    protected $logger;

    public function __construct(DatabaseConnection $connection, MixedLogger $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    public function searchAction()
    {
        $pdo = $this->connection->getPdo();
        $search = $_GET['s'] ?? null;
        $vm = new SearchViewModel();
        $vm->setSearchedText($search);
        View::SetViewModel($vm);

        if (!$search)
            return $this->renderView('search.php');

        $query = <<<SQL
SELECT p.*, COUNT(l.id) AS like_count, COUNT(d.id) AS relevance_score
FROM (SELECT id
      FROM (SELECT t.id, search_term, title_part
            FROM reflection.posts t
                     CROSS JOIN regexp_split_to_table(lower(:search), '\s+') search_term
                     CROSS JOIN regexp_split_to_table(lower(t.title), '\s+') title_part) result
               CROSS JOIN levenshtein_less_equal(result.search_term, result.title_part, :max_distance) distance
      WHERE distance <= :max_distance AND
            length(result.search_term) >= :min_length AND
            length(result.title_part) >= :min_length 
      LIMIT :limit) d
         INNER JOIN reflection.posts p ON d.id = p.id
         LEFT JOIN reflection.posts_likes l ON p.id = l.post_id
GROUP BY p.id
ORDER BY relevance_score DESC
SQL;

        $statement = $pdo->prepare($query);
        $statement->execute([
            ':search' => $search,
            ':max_distance' => static::MAX_LEVENSHTEIN_DISTANCE,
            ':min_length' => static::MIN_WORD_LENGTH,
            ':limit' => static::MAX_RESULTS
        ]);
        $statement->setFetchMode(PDO::FETCH_CLASS, Post::class);
        /** @var Post[] $posts */
        $posts = $statement->fetchAll();
        array_walk($posts, function ($post) use ($pdo) {
            /** @var Post $post */
            $post->setPdo($pdo);
        });

        $vm->setPosts($posts);

        return $this->renderView('search.php');
    }
}
