<?php

namespace App\Controller;

use App\Model\Category;
use App\Model\Post;
use App\OAuthSession;
use App\ViewModel\CategoryListViewModel;
use App\ViewModel\PostViewModel;
use Framework\Controller\Controller;
use Framework\Controller\RedirectResponse;
use Framework\Controller\RenderResponse;
use Framework\Controller\Response;
use Framework\Controller\ViewRenderResponse;
use Framework\DatabaseConnection;
use Framework\View\View;
use PDO;

class PostsController extends Controller
{
    /** @var int */
    protected const RELATED_POSTS_MAX_METAPHONE_THRESHOLD = 2;
    /** @var int */
    protected const MAX_RELATED_POSTS = 5;

    /** @var DatabaseConnection */
    protected $connection;

    /**
     * PostsController constructor.
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection;
    }

    /** @return ViewRenderResponse */
    public function categoriesAction(): ViewRenderResponse
    {
        $categories = iterator_to_array(Category::ReadAll($this->connection->getPdo()));
        $vm = new CategoryListViewModel();
        $vm->setCategories($categories);
        View::SetViewModel($vm);

        return $this->renderView('categories_list.php');
    }

    /**
     * @param string $category_name
     * @param int $post_id
     * @return RenderResponse|ViewRenderResponse
     */
    public function viewAction(string $category_name, int $post_id): Response
    {
        $pdo = $this->connection->getPdo();
        $post = $this->getPost($category_name, $post_id);
        if (null === $post)
            return $this->renderHttpStatusPage(404);

        $url = $post->getUrl();
        if ($_SERVER['REQUEST_URI'] !== $url)
            return $this->redirectTo($url, 301);

        $query = <<<SQL
SELECT posts.*, count(likes) AS like_count
FROM (SELECT v.id, v.relevance
      FROM (
               SELECT u.id, COUNT(u.id) AS relevance
               FROM (SELECT t.id,
                            MIN(levenshtein_less_equal(search.metaphone, titles.metaphone, :max_distance)) AS dist
                     FROM reflection.posts t
                              CROSS JOIN regexp_split_to_table(:metaphone, '\s+') search (metaphone) -- TODO: Replace :metaphone with a subquery based on :id
                              CROSS JOIN regexp_split_to_table(t.title_metaphone, '\s+') titles (metaphone)
                     WHERE t.id != :id AND
                           t.category_id = :category_id AND
                           length(titles.metaphone) > 1 AND
                           length(search.metaphone) > 1
                     GROUP BY search.metaphone, t.id
                    ) u
               WHERE u.dist < :max_distance
               GROUP BY u.id
               LIMIT :limit
           ) v
      ORDER BY v.relevance DESC
      LIMIT :limit) w
         INNER JOIN reflection.posts posts ON w.id = posts.id
         LEFT JOIN reflection.posts_likes likes ON likes.post_id = posts.id
GROUP BY posts.id
SQL;
        $statement = $pdo->prepare($query);
        $statement->execute([
            ':id' => $post->getId(),
            ':category_id' => $post->getCategoryId(),
            ':metaphone' => $post->getTitleMetaphone(),
            ':max_distance' => static::RELATED_POSTS_MAX_METAPHONE_THRESHOLD,
            ':limit' => static::MAX_RELATED_POSTS
        ]);
        $statement->setFetchMode(PDO::FETCH_CLASS, Post::class);
        /** @var Post[] $similar_posts */
        $similar_posts = $statement->fetchAll();
        array_walk($similar_posts, function ($post) use ($pdo) {
            /** @var Post $post */
            $post->setPdo($pdo);
        });

        $vm = new PostViewModel($post);
        $vm->setLiked($post->isLikedBy(OAuthSession::GetInstance()->getOAuthUserId()));
        $vm->setSimilarPosts($similar_posts);
        View::SetViewModel($vm);

        return $this->renderView('post.php');
    }

    /**
     * @param string $category_name
     * @param int $post_id
     * @return Post|null
     */
    protected function getPost(string $category_name, int $post_id): ?Post
    {
        $pdo = $this->connection->getPdo();
        $category = Category::ReadSingleBy($pdo, ['uri_identifier' => $category_name]);
        if (null === $category)
            return null;

        return Post::ReadSingleBy($pdo, ['id' => $post_id, 'category_id' => $category->getId()]);
    }

    /**
     * @param string $category_name
     * @param int $post_id
     * @return RedirectResponse|RenderResponse
     */
    public function likeAction(string $category_name, int $post_id): Response
    {
        $post = $this->getPost($category_name, $post_id);
        if (null === $post)
            return $this->renderHttpStatusPage(404);

        $post->setPdo($this->connection->getPdo());
        $user_id = OAuthSession::GetInstance()->getOAuthUserId();
        if ($user_id === null) {
            $encoded_url = urlencode($post->getUrl());
            return $this->redirectTo("/login?returnurl={$encoded_url}");
        }

        $statement = $this->connection->getPdo()->prepare('SELECT id FROM reflection.posts_likes WHERE user_id = :user_id AND post_id = :post_id LIMIT 1');
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $statement->execute([
            ':user_id' => $user_id,
            ':post_id' => $post->getId()
        ]);
        $result = $statement->fetch();

        $id = $result['id'];
        if (null === $id) {
            // Add like
            $statement = $this->connection->getPdo()->prepare('INSERT INTO reflection.posts_likes (user_id, provider, post_id)
                VALUES (:user_id, :provider, :post_id)');
            $statement->execute([
                ':user_id' => $user_id,
                ':provider' => OAuthSession::GetInstance()->getOAuthProvider(),
                ':post_id' => $post->getId()
            ]);
        } else {
            // Remove like
            $statement = $this->connection->getPdo()->prepare('DELETE FROM reflection.posts_likes WHERE id = :id');
            $statement->execute([':id' => $id]);
        }

        return $this->redirectTo($post->getUrl());
    }
}
