<?php

namespace App\Controller;

use App\Logging\MixedLogger;
use App\Model\Author;
use App\Model\Post;
use App\ViewModel\AuthorViewModel;
use Framework\Controller\Controller;
use Framework\Database\OrderedColumn;
use Framework\Database\QueryBuilderOperator as QBO;
use Framework\Database\QueryBuilderSelect;
use Framework\Database\TableJoin;
use Framework\DatabaseConnection;
use Framework\View\View;
use PDO;

class AuthorController extends Controller
{
    /** @var DatabaseConnection */
    protected $connection;
    /** @var MixedLogger */
    protected $logger;

    public function __construct(DatabaseConnection $connection, MixedLogger $logger)
    {
        $this->connection = $connection;
        $this->logger = $logger;
    }

    public function getAuthorAction(string $name)
    {
        $pdo = $this->connection->getPdo();
        $author = Author::ReadSingleBy($pdo, ['name' => $name]);
        if ($author === null)
            return $this->renderHttpStatusPage(404);

        $vm = new AuthorViewModel();
        $vm->setAuthor($author);
        $builder_base = QueryBuilderSelect::Create('reflection.posts', 'p')
            ->join('reflection.posts_likes l', TableJoin::LEFT, 'p.id', QBO::EQUAL, 'l.post_id')
            ->where('p.author_id', QBO::EQUAL, ':author_id')
            ->group('p.id')
            ->limit(':limit')
            ->select(['p.*', 'like_count' => 'COUNT(l.id)']);
        $statement_params = [
            ':author_id' => $author->getId(),
            ':limit' => AuthorViewModel::MAX_POSTS
        ];


        $best_builder = clone $builder_base;
        $best_builder->order('like_count', OrderedColumn::DESC);
        $statement = $pdo->prepare($best_builder->getQuery());
        $statement->execute($statement_params);
        $statement->setFetchMode(PDO::FETCH_CLASS, Post::class);
        /** @var Post[] $best */
        $best = $statement->fetchAll();
        array_walk($best, function ($post) use ($pdo) {
            /** @var Post $post */
            $post->setPdo($pdo);
        });

        $recent_builder = clone $builder_base;
        $recent_builder->order('date', OrderedColumn::DESC);
        $statement = $pdo->prepare($recent_builder->getQuery());
        $statement->execute($statement_params);
        $statement->setFetchMode(PDO::FETCH_CLASS, Post::class);
        /** @var Post[] $recent */
        $recent = $statement->fetchAll();
        array_walk($recent, function ($post) use ($pdo) {
            /** @var Post $post */
            $post->setPdo($pdo);
        });

        $vm->setBestPosts($best);
        $vm->setRecentPosts($recent);
        View::SetViewModel($vm);

        return $this->renderView('author.php');
    }
}
