<?php

namespace App\Controller;

use App\Model\Author;
use Framework\Controller\Controller;
use Framework\Controller\RedirectResponse;
use Framework\Controller\ViewRenderResponse;
use Framework\DatabaseConnection;
use Framework\Session\Session;
use Framework\View\View;
use Framework\ViewModel\ViewModel;
use L;

class AuthorLoginController extends Controller
{
    /** @var DatabaseConnection */
    protected $connection;

    /**
     * AuthorLoginController constructor.
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        if (Session::GetInstance()->isLoggedIn()) {
            $this->redirectTo('/');
            exit;
        }

        $this->connection = $connection;
    }

    /**
     * @return ViewRenderResponse
     */
    public function getLoginAction(): ViewRenderResponse
    {
        View::SetViewModel(new ViewModel());
        return $this->renderView('author_login.php');
    }


    /** @return RedirectResponse */
    public function postLoginAction(): RedirectResponse
    {
        $errors_bag = Session::GetInstance()->getErrorsBag();
        if (false === $this->checkCsrfToken()) {
            $errors_bag[] = L::errors_invalid_csrf_token;
            return $this->reload();
        }

        $email = $_REQUEST['email'] ?? '';
        $password = $_REQUEST['password'] ?? '';
        $author = Author::ReadSingleBy($this->connection->getPdo(), ['email' => $email]);
        if (null === $author) {
            // Invalid email
            $errors_bag[] = L::dashboard_errors_login_invalid_credentials;
            return $this->reload();
        }

        if (false === password_verify($password, $author->getPasswordHash())) {
            // Invalid password
            $errors_bag[] = L::dashboard_errors_login_invalid_credentials;
            return $this->reload();
        }

        Session::GetInstance()->setUserId($author->getId());

        return $this->redirectTo('/');
    }
}
