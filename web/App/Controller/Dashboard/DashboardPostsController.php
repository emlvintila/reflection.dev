<?php

namespace App\Controller\Dashboard;

use App\Model\Category;
use App\Model\Post;
use App\ViewModel\CrudViewModel;
use App\ViewModel\PostCrudViewModel;
use Framework\Controller\RenderResponse;
use Framework\Controller\Response;
use Framework\DatabaseConnection;
use Framework\Form\FormHelper;
use Framework\View\View;
use InvalidArgumentException;
use L;

class DashboardPostsController extends DashboardController
{
    /**
     * DashboardPostsController constructor.
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        parent::__construct($connection);
    }

    /**
     * @param string|null $crud_action
     * @param int|null $id
     * @return RenderResponse
     */
    public function getAction(?string $crud_action = null, ?int $id = null): RenderResponse
    {
        try {
            $vm = new PostCrudViewModel($crud_action);
        } catch (InvalidArgumentException $exception) {
            return $this->renderHttpStatusPage(400);
        }
        View::SetViewModel($vm);
        if (false === $this->session->isFromPost()) {
            switch ($vm->getCrudAction()) {
                case CrudViewModel::ACTION_CREATE:
                    $vm->setEntity(new Post());
                    break;
                case CrudViewModel::ACTION_UPDATE:
                case CrudViewModel::ACTION_READ:
                case CrudViewModel::ACTION_DELETE:
                    if (null === $id)
                        return $this->renderHttpStatusPage(400);

                    $post = Post::ReadSingle($this->connection->getPdo(), $id);
                    if (null === $post)
                        return $this->renderHttpStatusPage(404);

                    if ($post->getAuthorId() !== $this->author->getId())
                        return $this->renderHttpStatusPage(403);

                    $vm->setEntity($post);
                    break;
            }
            if ($vm->isCreateOrUpdate())
                $vm->setCategories(iterator_to_array(Category::ReadAll($this->connection->getPdo())));
        }

        return $this->renderView('dashboard/post.php');
    }

    /**
     * @param string|null $crud_action
     * @param int|null $id
     * @return Response
     */
    public function postAction(?string $crud_action, ?int $id = null): Response
    {
        $this->session->setFromPost(true);
        $errors = $this->session->getErrorsBag();
        if (false === $this->checkCsrfToken()) {
            $errors[] = L::errors_invalid_csrf_token;
            return $this->reload();
        }

        /** @var Post $post */
        $post = FormHelper::CreateEntityFromRequest($_REQUEST['Post'], Post::class);
        $post->setPdo($this->connection->getPdo());

        switch (strtoupper($crud_action)) {
            case CrudViewModel::ACTION_CREATE:
                $post->setAuthorId($this->author->getId());
                if ($model_errors = $post->getModelStateErrors()) {
                    foreach ($model_errors as $model_error)
                        $errors[] = $model_error;

                    return $this->reload();
                }

                $post->createIntoDatabase();
                return $this->redirectTo($post->getUrl());
            case CrudViewModel::ACTION_UPDATE:
                if (null === $id || null === $db_post = Post::ReadSingle($this->connection->getPdo(), $id)) {
                    $errors[] = sprintf(L::dashboard_errors_posts_invalid_id, $id);

                    return $this->reload();
                }

                if ($db_post->getAuthorId() !== $this->author->getId()) {
                    $errors[] = sprintf(L::dashboard_errors_posts_not_author, $id);

                    return $this->reload();
                }

                $post->setId($id);
                $post->setAuthorId($this->author->getId());

                if ($model_errors = $post->getModelStateErrors()) {
                    foreach ($model_errors as $model_error)
                        $errors[] = $model_error;

                    return $this->reload();
                }

                $post->updateIntoDatabase();

                return $this->redirectTo($post->getUrl());
            case CrudViewModel::ACTION_DELETE:
                if (null === $id || null === Post::ReadSingle($this->connection->getPdo(), $id)) {
                    $errors[] = sprintf(L::dashboard_errors_posts_invalid_id, $id);

                    return $this->reload();
                }

                $post->setId($id);
                if (false === $post->deleteFromDatabase()) {
                    $errors[] = sprintf(L::dashboard_errors_posts_cant_delete, $id);

                    return $this->reload();
                }

                return $this->redirectTo('/dashboard/posts');
            default:
                return $this->renderHttpStatusPage(400);
        }
    }
}
