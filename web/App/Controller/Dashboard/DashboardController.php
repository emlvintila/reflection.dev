<?php

namespace App\Controller\Dashboard;

use App\Model\Author;
use Framework\Controller\Controller;
use Framework\DatabaseConnection;
use Framework\Session\Session;

abstract class DashboardController extends Controller
{
    /** @var DatabaseConnection */
    protected $connection;
    /** @var Session */
    protected $session;
    /** @var Author */
    protected $author;

    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection;
        $this->session = Session::GetInstance();
        $id = $this->session->getUserId();
        if (null === $id || null === $this->author = Author::ReadSingle($this->connection->getPdo(), $id)) {
            $this->redirectTo('/author-login');
            exit;
        }
    }
}
