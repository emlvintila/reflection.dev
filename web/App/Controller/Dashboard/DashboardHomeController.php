<?php

namespace App\Controller\Dashboard;

use Framework\Controller\ViewRenderResponse;
use Framework\DatabaseConnection;

class DashboardHomeController extends DashboardController
{
    /**
     * DashboardHomeController constructor.
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        parent::__construct($connection);
    }

    /** @return ViewRenderResponse */
    public function defaultAction(): ViewRenderResponse
    {
        return $this->renderView('dashboard/home.php');
    }
}
