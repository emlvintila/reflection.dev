<?php

namespace App\Controller;

use App\Logging\MixedLogger;
use App\OAuth\FacebookHelper;
use App\OAuth\GoogleHelper;
use App\OAuth\TwitterHelper;
use App\OAuthSession;
use App\ViewModel\LoginViewModel;
use Exception;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Framework\Controller\Controller;
use Framework\Controller\RedirectResponse;
use Framework\Controller\RenderResponse;
use Framework\Controller\ViewRenderResponse;
use Framework\URL;
use Framework\View\View;
use Google_Service_Oauth2;
use TwitterAPIExchange;

class LoginController extends Controller
{
    /** @var MixedLogger */
    protected $logger;
    /** @var Facebook */
    protected $fb;

    /**
     * LoginController constructor.
     * @param MixedLogger $logger
     */
    public function __construct(MixedLogger $logger)
    {
        if (OAuthSession::GetInstance()->isOAuthLoggedIn()) {
            $this->redirectTo('/');
            exit;
        }

        $this->logger = $logger;
    }

    /**
     * @return RenderResponse|ViewRenderResponse
     */
    public function getLoginAction()
    {
        $vm = new LoginViewModel();
        try {
            $fb = FacebookHelper::GetFacebook();

            $helper = $fb->getRedirectLoginHelper();
            $fb_permissions = ['public_profile', 'email'];
            $fb_login_url = $helper->getLoginUrl(URL::FromRoot('/fb-callback'), $fb_permissions);
            $vm->setFacebookLoginUrl($fb_login_url);
        } catch (FacebookSDKException $e) {
            $this->logger->error($e->getMessage());
        }

        try {
            $google = GoogleHelper::GetClient();
            $google->setRedirectUri(URL::FromRoot('/google-callback'));
            $google->addScope([Google_Service_Oauth2::USERINFO_EMAIL, Google_Service_Oauth2::USERINFO_PROFILE]);
            $vm->setGoogleLoginUrl($google->createAuthUrl());
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }

        $vm->setTwitterLoginUrl('/twitter-login');
        View::SetViewModel($vm);

        return $this->renderView('login.php');
    }

    public function postLoginAction()
    {

    }

    /**
     * @return RedirectResponse|RenderResponse
     * @throws Exception
     */
    public function getTwitterLoginAction()
    {
        $tw = new TwitterAPIExchange(TwitterHelper::GetSettings());
        $callback_url = urlencode(URL::FromRoot('/twitter-callback'));
        $response = $tw
            ->buildOauth('https://api.twitter.com/oauth/request_token', 'POST')
            ->setPostfields(['oauth_callback' => $callback_url])
            ->performRequest();

        list(
            $oauth_token,
            $oauth_token_secret,
            $confirmed
            ) = array_map(function ($s) {
            return explode('=', $s, 2)[1];
        }, explode('&', $response));

        if ($confirmed != 'true')
            return $this->renderHttpStatusPage(400);

        OAuthSession::GetInstance()->setOAuthToken($oauth_token);
        OAuthSession::GetInstance()->setOAuthTokenSecret($oauth_token_secret);

        return $this->redirectTo("https://api.twitter.com/oauth/authenticate?oauth_token={$oauth_token}");
    }
}
