<?php

namespace App\Controller;

use Framework\Controller\Controller;
use Framework\Controller\ViewRenderResponse;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    /** @return ViewRenderResponse */
    public function defaultAction(): ViewRenderResponse
    {
        return $this->renderView('home.php');
    }
}
