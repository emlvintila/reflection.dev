<?php

namespace App\Controller;

use App\Model\Category;
use App\Model\MailingListUser;
use App\Model\Post;
use App\ViewModel\CategoryViewModel;
use App\ViewModel\PostListViewModel;
use Framework\Controller\Controller;
use Framework\Controller\RedirectResponse;
use Framework\Controller\RenderResponse;
use Framework\Controller\Response;
use Framework\Controller\ViewRenderResponse;
use Framework\Database\OrderedColumn;
use Framework\DatabaseConnection;
use Framework\Session\Session;
use Framework\View\View;
use L;

class CategoriesController extends Controller
{
    /** @var int */
    protected const ITEMS_PER_PAGE = 25;

    /** @var DatabaseConnection */
    protected $connection;

    /**
     * CategoriesController constructor.
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param string $category_name
     * @return RenderResponse|ViewRenderResponse
     */
    public function getAction(string $category_name): RenderResponse
    {
        $pdo = $this->connection->getPdo();
        $category = Category::ReadFromDatabaseByUriId($pdo, $category_name);
        if (null === $category)
            return $this->renderHttpStatusPage(404);

        $vm = new CategoryViewModel();
        $vm->setCategory($category);
        View::SetViewModel($vm);

        return $this->renderView('category.php');
    }

    /**
     * @param string $category_name
     * @param int|null $page
     * @return RenderResponse|ViewRenderResponse
     */
    public function getPostsListAction(string $category_name, ?int $page = null): RenderResponse
    {
        $pdo = $this->connection->getPdo();
        $category = Category::ReadFromDatabaseByUriId($pdo, $category_name);
        if (null === $category)
            return $this->renderHttpStatusPage(404);

        if (null === $page || !$page || $page < 1)
            $page = 1;

        $posts = iterator_to_array(Post::ReadMultipleBy($pdo, ['category_id' => $category->getId()],
            ['id' => OrderedColumn::DESC], ($page - 1) * static::ITEMS_PER_PAGE, static::ITEMS_PER_PAGE));

        $page_count = (int)ceil((float)Post::GetCount($pdo) / static::ITEMS_PER_PAGE);

        $vm = new PostListViewModel();
        $vm->setCategory($category);
        $vm->setPosts($posts);

        $vm->setCurrentPage($page);
        $vm->setMaxPage($page_count);
        $vm->setHasPrev($page > 1);
        $vm->setHasNext($page < $page_count);
        $vm->setTotalPosts(Post::GetCount($pdo));

        View::SetViewModel($vm);

        return $this->renderView('posts_list.php');
    }

    /**
     * @param string $category_name
     * @return RedirectResponse|RenderResponse
     */
    public function postSubscribeAction(string $category_name): Response
    {
        $pdo = $this->connection->getPdo();
        $session = Session::GetInstance();
        $session->setFromPost(true);
        $errors = $session->getErrorsBag();
        $success = $session->getSuccessBag();
        $category = Category::ReadFromDatabaseByUriId($pdo, $category_name);
        if (null === $category)
            return $this->renderHttpStatusPage(400);

        $name = $_REQUEST['name'];
        $email = $_REQUEST['email'];

        $existing_user = MailingListUser::ReadSingleBy($pdo, ['email' => $email]);

        if (null !== $existing_user) {
            $errors[] = sprintf(L::errors_categories_subscription_already, $category->getFriendlyName());
            return $this->redirectTo($category->getUrl());
        }

        $user = new MailingListUser();
        $user->setPdo($pdo);
        $user->setName($name);
        $user->setEmail($email);
        $user->setCategoryId($category->getId());
        if ($model_errors = $user->getModelStateErrors()) {
            foreach ($model_errors as $model_error)
                $errors[] = $model_error;
        } else {
            $user->createIntoDatabase();
            $success[] = sprintf(L::success_categories_subscription_success, $category->getFriendlyName());
        }

        return $this->redirectTo($category->getUrl());
    }

    /**
     * @param string $category_name
     * @return RenderResponse|ViewRenderResponse
     */
    public function getUnsubscribeAction(string $category_name): RenderResponse
    {
        $pdo = $this->connection->getPdo();
        $category = Category::ReadFromDatabaseByUriId($pdo, $category_name);
        if (null === $category)
            return $this->renderHttpStatusPage(404);

        $vm = new CategoryViewModel();
        $vm->setCategory($category);
        View::SetViewModel($vm);

        return $this->renderView('category_unsubscribe.php');
    }

    public function postUnsubscribeAction(string $category_name)
    {
        $pdo = $this->connection->getPdo();
        $category = Category::ReadFromDatabaseByUriId($pdo, $category_name);
        if (null === $category)
            return $this->renderHttpStatusPage(400);

        $session = Session::GetInstance();
        $session->setFromPost(true);
        $errors = $session->getErrorsBag();
        $success = $session->getSuccessBag();
        $email = $_REQUEST['email'];

        $user = MailingListUser::ReadSingleBy($pdo, ['email' => $email]);
        if (null === $user) {
            $errors[] = sprintf(L::errors_categories_unsubscribe_not, $category->getFriendlyName());
        } else {
            $user->deleteFromDatabase();
            $success[] = sprintf(L::success_categories_unsubscribe_success, $category->getFriendlyName());
        }

        return $this->reload();
    }
}
