<?php

namespace App;

use Framework\Session\Session;
use InvalidArgumentException;

class OAuthSession extends Session
{
    /** @var string */
    public const AUTH_PROVIDER_FACEBOOK = 'facebook';
    public const AUTH_PROVIDER_TWITTER = 'twitter';
    public const AUTH_PROVIDER_INSTAGRAM = 'instagram';
    public const AUTH_PROVIDER_GITHUB = 'github';
    public const AUTH_PROVIDER_GOOGLE = 'google';
    /** @var string[] */
    protected const AUTH_PROVIDERS = [
        self::AUTH_PROVIDER_FACEBOOK,
        self::AUTH_PROVIDER_TWITTER,
        self::AUTH_PROVIDER_INSTAGRAM,
        self::AUTH_PROVIDER_GITHUB,
        self::AUTH_PROVIDER_GOOGLE,
        null
    ];
    /** @var string|null */
    protected ?string $oauth_provider;
    /** @var string|null */
    protected ?string $oauth_token;
    /** @var string|null */
    protected ?string $oauth_token_secret;
    /** @var string|null */
    protected ?string $oauth_user_id;
    /** @var string|null */
    protected ?string $oauth_user_name;
    /** @var bool */
    protected bool $gtag_oauth_login_sent;
    /** @var bool */
    protected bool $gtag_author_login_sent;

    protected function __construct()
    {
        parent::__construct();
        $this->oauth_provider = $_SESSION['oauth.provider'] ?? null;
        $this->oauth_token = $_SESSION['oauth.token'] ?? null;
        $this->oauth_token_secret = $_SESSION['oauth.token_secret'] ?? null;
        $this->oauth_user_id = $_SESSION['oauth.user.id'] ?? null;
        $this->oauth_user_name = $_SESSION['oauth.user.name'] ?? null;
        $this->gtag_oauth_login_sent = $_SESSION['gtag.oauth_login_sent'] ?? false;
        $this->gtag_author_login_sent = $_SESSION['gtag.author_login_sent'] ?? false;
    }

    /** @return string|null */
    public function getOAuthToken(): ?string
    {
        return $this->oauth_token;
    }

    /** @param string|null $oauth_token */
    public function setOAuthToken(?string $oauth_token): void
    {
        $this->oauth_token = $_SESSION['oauth.token'] = $oauth_token;
    }

    /** @return string|null */
    public function getOAuthTokenSecret(): ?string
    {
        return $this->oauth_token_secret;
    }

    /** @param string|null $oauth_token_secret */
    public function setOAuthTokenSecret(?string $oauth_token_secret): void
    {
        $this->oauth_token_secret = $_SESSION['oauth.token_secret'] = $oauth_token_secret;
    }

    /** @return string|null */
    public function getOAuthUserId(): ?string
    {
        return $this->oauth_user_id;
    }

    /** @param string|null $oauth_user_id */
    public function setOAuthUserId(?string $oauth_user_id): void
    {
        $this->oauth_user_id = $_SESSION['oauth.user.id'] = $oauth_user_id;
    }

    /** @return string|null */
    public function getOAuthUserName(): ?string
    {
        return $this->oauth_user_name;
    }

    /** @param string|null $oauth_user_name */
    public function setOAuthUserName(?string $oauth_user_name): void
    {
        $this->oauth_user_name = $_SESSION['oauth.user.name'] = $oauth_user_name;
    }

    /**
     * @return string The gtag function call for an OAuth login event
     * if the even't hasn't been previously sent during this session.
     */
    public function checkAndSendOAuthGtagLogin(): string
    {
        if ($this->isOAuthLoggedIn() && false === $this->gtag_oauth_login_sent) {
            $this->gtag_oauth_login_sent = true;
            return "gtag('event', 'login', {'method': '{$this->oauth_provider}'}));";
        }
        return '';
    }

    public function isOAuthLoggedIn(): bool
    {
        return $this->getOAuthProvider() !== null;
    }

    /** @return string|null */
    public function getOAuthProvider(): ?string
    {
        return $this->oauth_provider;
    }

    /** @param string|null $auth_provider */
    public function setOAuthProvider(?string $auth_provider): void
    {
        if (false === array_search($auth_provider, static::AUTH_PROVIDERS))
            throw new InvalidArgumentException();

        $this->oauth_provider = $_SESSION['oauth.provider'] = $auth_provider;
    }

    /**
     * @return string The gtag function call for an author login event
     * if the even't hasn't been previously sent during this session.
     */
    public function checkAndSendAuthorGtagLogin(): string
    {
        if ($this->isOAuthLoggedIn() && false === $this->gtag_author_login_sent) {
            $this->gtag_author_login_sent = true;
            return "gtag('event', 'login', {'method': 'author_login'}));";
        }
        return '';
    }

    public function checkAndSendGtagUserId()
    {
        if ($this->isOAuthLoggedIn()) {
            return "gtag('config', 'UA-56329527-2', {
                'user_id': '{$this->oauth_user_id}'
            });";
        }
        return '';
    }
}
