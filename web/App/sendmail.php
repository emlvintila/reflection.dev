<?php

use App\Model\Category;
use App\Model\MailingListUser;
use App\Model\Post;
use Framework\App;
use Framework\Database\OrderedColumn;
use Framework\Database\QueryBuilderOperator;
use Framework\DatabaseConnection;
use Framework\Logging\FileLogger;
use PHPMailer\PHPMailer\PHPMailer;

require __DIR__ . '/bootstrap.php';

const MAX_POSTS = 5;

$app = App::GetInstance();
$app->configureFrom(__DIR__ . DIRECTORY_SEPARATOR . '../config_dev.json');
/** @var DatabaseConnection $db */
$db = $app->getDatabaseConnections()['default'];
$pdo = $db->getPdo();

$mail = new PHPMailer(true);
$mail->SMTPDebug = 2;
/** @noinspection PhpUnhandledExceptionInspection */
$mail->setFrom('noreply@reflection.to', 'noreply@reflection.to');
$mail->CharSet = PHPMailer::CHARSET_UTF8;
$mail->isHTML(true);

$cache_dir = join(DIRECTORY_SEPARATOR, [__DIR__, '..', '.sendmail']);
if (false === file_exists($cache_dir))
    mkdir($cache_dir, 0755, true);
$prev_time_file = join(DIRECTORY_SEPARATOR, [$cache_dir, '.last']);
$prev_time = file_exists($prev_time_file) ?
    unserialize(file_get_contents($prev_time_file)) :
    new DateTime('1999-03-17');
$logger = new FileLogger(join(DIRECTORY_SEPARATOR, [$cache_dir, 'error.log']));

$query = Post::GetSelectQueryBuilder()
    ->where('t.category_id', QueryBuilderOperator::EQUAL, ':category_id')
    ->where('t.date', QueryBuilderOperator::GREATER_THAN, ':last')
    ->order('t.date', OrderedColumn::DESC)
    ->limit(':limit')
    ->getQuery();
$statement = $pdo->prepare($query);
$statement->setFetchMode(PDO::FETCH_CLASS, Post::class);

$categories = Category::ReadAll($pdo);
/** @var Category $category */
foreach ($categories as $category) {
    $mail->Subject = sprintf(L::email_subject, $category->getFriendlyName());
    $statement->execute([
        ':category_id' => $category->getId(),
        ':last' => $prev_time->format(DATE_ISO8601),
        ':limit' => MAX_POSTS
    ]);
    $latest_posts = $statement->fetchAll();
    if (!$latest_posts)
        continue;
    array_walk($latest_posts, function ($post) use ($pdo) {
        /** @var Post $post */
        $post->setPdo($pdo);
    });
    $users = MailingListUser::ReadMultipleBy($pdo, ['category_id' => $category->getId()]);
    /** @var MailingListUser $user */
    foreach ($users as $user) {
        try {
            $mail->clearAddresses();
            $mail->addAddress($user->getEmail(), $user->getName());

            $body = renderTemplate(__DIR__ . DIRECTORY_SEPARATOR . '../EmailTemplates/template.php', $category, $user, $latest_posts);

            $mail->Body = $body;
            $mail->AltBody = '';

            $html_file = join(DIRECTORY_SEPARATOR,
                [$cache_dir, '.mail.' . md5($body) . '.html']);
            file_put_contents($html_file, $body);

            $mail->send();
        } catch (Exception $exception) {
            $logger->error($exception->getMessage());
        }
    }
}

file_put_contents($prev_time_file, serialize(new DateTime()));

/**
 * @param string $filename
 * @param Category $category
 * @param MailingListUser $user
 * @param Post[] $posts
 * @return string
 */
function renderTemplate(string $filename,
    /** @noinspection PhpUnusedParameterInspection */ Category $category,
    /** @noinspection PhpUnusedParameterInspection */ MailingListUser $user,
    /** @noinspection PhpUnusedParameterInspection */ array $posts): string
{
    ob_start();
    /** @noinspection PhpIncludeInspection */
    require $filename;
    return ob_get_clean();
}
