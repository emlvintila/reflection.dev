<?php

namespace App;

use Framework\Session\Session;
use L;

class Utils
{
    /**
     * @param string $text
     * @param int $wpm Words per minute
     * @return string
     */
    public static function ReadTime(string $text, int $wpm = 150): string
    {
        $words = (float)str_word_count($text);
        $minutes = ceil($words / $wpm);
        if ($minutes < 0.5)
            return sprintf(L::read_time_lt_one_min, 1);

        if ($minutes < 1.5)
            return sprintf(L::read_time_one_min, 1);

        return sprintf(L::read_time_minutes,
            sprintf('%d', ceil($minutes)));
    }

    /**
     * @return string
     */
    public static function RenderNavLinks(): string
    {
        $logout_text = sprintf(L::nav_logout, OAuthSession::GetInstance()->getOAuthUserName());

        return join([
            !Session::GetInstance()->isLoggedIn() ? '' :
                static::MakeNavLink('/dashboard', L::nav_dashboard_dashboard, L::nav_dashboard_dashboard,
                    static::MakeIcon('fa-chart-line')),
            static::MakeNavLink('/search', L::nav_search, L::nav_search,
                static::MakeIcon('fa-search')),
            OAuthSession::GetInstance()->isOAuthLoggedIn() ?
                static::MakeNavLink('/logout', $logout_text, $logout_text,
                    static::MakeIcon('fa-sign-out-alt')) :
                static::MakeNavLink('/login', L::nav_login, L::nav_login,
                    static::MakeIcon('fa-sign-in-alt'))
        ]);
    }

    /**
     * @param string $href
     * @param string $title
     * @param string $text
     * @param string $icon
     * @return string
     */
    protected static function MakeNavLink(string $href, string $title, string $text, string $icon): string
    {
        return "<li><a class=\"white-text\" href=\"{$href}\" title=\"{$title}\">{$text} {$icon}</a></li>";
    }

    protected static function MakeIcon(string $icon, string $type = "fas")
    {
        return "<i class=\"{$type} {$icon} white-text mr-0\"></i>";
    }

    /**
     * @return string
     */
    public static function RenderDashboardNavLinks(): string
    {
        if (false === Session::GetInstance()->isLoggedIn())
            return '';

        return join([
            static::MakeNavLink('/dashboard/posts/create', L::nav_dashboard_new_post, L::nav_dashboard_new_post,
                static::MakeIcon('fa-paper-plane'))
        ]);
    }

    /**
     * @param array $errors
     * @param string|null $header
     * @return string
     */
    public static function RenderErrors(array $errors, ?string $header = null): string
    {
        return static::RenderMessages($errors, 'red', $header);
    }

    public static function RenderSuccess(array $success, ?string $header = null): string
    {
        return static::RenderMessages($success, 'green', $header);
    }

    protected static function RenderMessages(array $messages, string $panel_class, ?string $header = null): string
    {
        if (count($messages) === 0)
            return '';

        $html = "<div style=\"padding: 6px;\" class=\"card-panel {$panel_class}\"><ul class=\"collection mt-0 mb-0\">";
        if ($header)
            $html .= "<li class=\"collection-header\"><h4>{\$header}</h4></li>";
        foreach ($messages as $message)
            $html .= "<li class=\"collection-item\">{$message}</li>";
        $html .= "</ul></div>";

        return $html;
    }
}
