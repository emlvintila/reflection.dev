<?php

namespace App\Logging;

use Framework\Logging\DatabaseLogger;
use PDO;

class DefaultDatabaseLogger extends DatabaseLogger
{
    /**
     * @param int $severity
     * @param array $args
     * @return bool
     */
    public function log(int $severity, ...$args): bool
    {
        $statement = $this->connection->getPdo()->prepare('INSERT INTO reflection_logs.logs (text, severity) VALUES (:text, :severity)');
        if (false === $statement)
            return false;

        $statement->bindValue(':text', serialize($args));
        $statement->bindValue(':severity', $severity, PDO::PARAM_INT);
        if (false === $statement->execute())
            return false;

        return true;
    }
}
