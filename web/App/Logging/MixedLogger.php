<?php

namespace App\Logging;

use Framework\Logging\DatabaseLogger;
use Framework\Logging\FileLogger;
use Framework\Logging\Logger;

class MixedLogger extends Logger
{
    /** @var DatabaseLogger */
    protected $databaseLogger;
    /** @var FileLogger */
    protected $fileLogger;

    public function __construct(DatabaseLogger $databaseLogger, FileLogger $fileLogger)
    {
        $this->databaseLogger = $databaseLogger;
        $this->fileLogger = $fileLogger;
    }

    /**
     * @param int $severity
     * @param array $args
     */
    public function log(int $severity, ...$args)
    {
        if (false === $this->databaseLogger->log($severity, $args))
            $this->fileLogger->log($severity, $args);
    }
}
