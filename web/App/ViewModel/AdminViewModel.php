<?php

namespace App\ViewModel;

use Framework\ViewModel\ViewModel;

class AdminViewModel extends ViewModel
{
    /** @var string */
    protected $api_key;

    protected function __construct()
    {
    }

    /** @return string */
    public function getApiKey(): string
    {
        return $this->api_key;
    }

    /** @param string $api_key */
    public function setApiKey(string $api_key): void
    {
        $this->api_key = $api_key;
    }
}
