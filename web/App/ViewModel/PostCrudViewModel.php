<?php

namespace App\ViewModel;

use App\Model\Category;

class PostCrudViewModel extends CrudViewModel
{
    /** @var Category[] */
    protected $categories = [];

    /** @return Category[] */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /** @param Category[] $categories */
    public function setCategories(array $categories): void
    {
        $this->categories = $categories;
    }
}
