<?php

namespace App\ViewModel;

use App\Model\Category;
use Framework\ViewModel\ViewModel;
use L;

class CategoryViewModel extends ViewModel
{
    /** @var Category */
    protected $category;

    /** @return Category */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /** @param Category $category */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    public static function RenderSubscribeForm(Category $category)
    {
        $name_label = L::posts_subscribe_inputs_name;
        $email_label = L::posts_subscribe_inputs_email;
        $button_label = L::posts_subscribe_inputs_submit;
        $gtag_label = addslashes($category->getUriIdentifier());

        return <<<HTML
<form id="subscribe-form" method="POST" action="{$category->getSubscribeUrl()}">
    <div class="input-field">
        <input required type="text" id="name" name="name" />
        <label for="name">{$name_label}</label>
    </div>
    <div class="input-field">
        <input required type="email" id="email" name="email" />
        <label for="email">{$email_label}</label>
    </div>
    
    <button id="subscribe-button" type="submit" class="right btn light-blue waves-effect">{$button_label} <i class="fas fa-bell"></i></button>
</form>

<script type="text/javascript">
        document.querySelector('#subscribe-form').addEventListener('submit', () =>
            gtag('event', 'subscribe', {
                'event_category': 'subscription',
                'event_label': '{$gtag_label}'
            })
        );
    </script>
HTML;
    }

    public static function RenderUnsubscribeForm(Category $category)
    {
        $email = $_REQUEST['email'] ?? '';
        $email_label = L::posts_subscribe_inputs_email;
        $button_label = L::categories_subscription_inputs_unsubscribe_submit;
        $gtag_label = addslashes($category->getUriIdentifier());

        return <<<HTML
<form id="unsubscribe-form" method="POST" action="{$category->getUnsubscribeUrl()}">
    <div class="input-field">
        <input required type="email" id="email" name="email" value="{$email}" />
        <label for="email">{$email_label}</label>    
    </div>
    
    <button id="unsubscribe-button" type="submit" class="right btn red waves-effect">{$button_label} <i class="fas fa-bell-slash"></i></button>
</form>

<script type="text/javascript">
    document.querySelector('#unsubscribe-form').addEventListener('submit',
        () => gtag('event', 'unsubscribe', {
            'event_category': 'subscription',
            'event_label': '{$gtag_label}'
        }));
</script>
HTML;

    }
}
