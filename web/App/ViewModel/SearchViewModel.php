<?php

namespace App\ViewModel;

class SearchViewModel extends PostListViewModel
{
    /** @var string|null */
    protected $searched_text;

    /** @return string|null */
    public function getSearchedText(): ?string
    {
        return $this->searched_text;
    }

    /** @param string|null $searched_text */
    public function setSearchedText(?string $searched_text): void
    {
        $this->searched_text = $searched_text;
    }

    public function hasQuery()
    {
        return (bool) $this->searched_text;
    }

    public function hasResults()
    {
        return (bool) $this->posts;
    }
}
