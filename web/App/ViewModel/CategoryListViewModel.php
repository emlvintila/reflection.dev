<?php

namespace App\ViewModel;

use App\Model\Category;
use Framework\ViewModel\ViewModel;

class CategoryListViewModel extends ViewModel
{
    /** @var Category[] */
    protected $categories;

    /** @return Category[] */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /** @param Category[] $categories */
    public function setCategories(array $categories): void
    {
        $this->categories = $categories;
    }

    /** @param Category $category */
    public function addCategory(Category $category)
    {
        $this->categories[] = $category;
    }
}
