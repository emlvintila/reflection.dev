<?php

namespace App\ViewModel;

use Framework\ViewModel\ViewModel;

class LoginViewModel extends ViewModel
{
    /** @var string|null */
    protected $facebook_login_url;
    /** @var string|null */
    protected $twitter_login_url;
    /** @var string|null */
    protected $google_login_url;

    /** @return string|null */
    public function getFacebookLoginUrl(): ?string
    {
        return $this->facebook_login_url;
    }

    /** @param string|null $facebook_login_url */
    public function setFacebookLoginUrl(?string $facebook_login_url): void
    {
        $this->facebook_login_url = $facebook_login_url;
    }

    /** @return string|null */
    public function getTwitterLoginUrl(): ?string
    {
        return $this->twitter_login_url;
    }

    /** @param string|null $twitter_login_url */
    public function setTwitterLoginUrl(?string $twitter_login_url): void
    {
        $this->twitter_login_url = $twitter_login_url;
    }

    /** @return string|null */
    public function getGoogleLoginUrl(): ?string
    {
        return $this->google_login_url;
    }

    /** @param string|null $google_login_url */
    public function setGoogleLoginUrl(?string $google_login_url): void
    {
        $this->google_login_url = $google_login_url;
    }
}
