<?php

namespace App\ViewModel;

use App\Model\Category;
use App\Model\Post;
use App\Utils;
use Framework\URL;
use Framework\ViewModel\ViewModel;
use L;

class PostListViewModel extends ViewModel
{
    /** @var bool */
    protected $has_prev;
    /** @var bool */
    protected $has_next;
    /** @var int */
    protected $current_page;
    /** @var int */
    protected $max_page;
    /** @var int */
    protected $total_posts;
    /** @var Post[] */
    protected $posts = [];
    /** @var Category */
    protected $category;

    /** @return Post[] */
    public function getPosts(): array
    {
        return $this->posts;
    }

    /** @param Post[] $posts */
    public function setPosts(array $posts): void
    {
        $this->posts = $posts;
    }

    /** @return Category */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /** @param Category $category */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /** @param Post $post */
    public function addPost(Post $post): void
    {
        $this->posts[] = $post;
    }

    /** @param bool $has_prev */
    public function setHasPrev(bool $has_prev): void
    {
        $this->has_prev = $has_prev;
    }

    /** @param bool $has_next */
    public function setHasNext(bool $has_next): void
    {
        $this->has_next = $has_next;
    }

    /** @return int */
    public function getCurrentPage(): int
    {
        return $this->current_page;
    }

    /** @param int $current_page */
    public function setCurrentPage(int $current_page): void
    {
        $this->current_page = $current_page;
    }

    /** @return int */
    public function getMaxPage(): int
    {
        return $this->max_page;
    }

    /** @param int $max_page */
    public function setMaxPage(int $max_page): void
    {
        $this->max_page = $max_page;
    }

    /** @return string */
    public function renderAll(): string
    {
        $html = '';
        foreach ($this->posts as $post)
            $html .= PostListViewModel::RenderPost($post);

        return $html;
    }

    /**
     * @param Post $post
     * @return string
     */
    public static function RenderPost(Post $post): string
    {
        $word_count = sprintf(L::posts_word_count, str_word_count($post->getBody()));
        $html = '<div class="row"><div class="col s12">';
        $html .= '<p class="mb-0">';
        $html .= "<span class=\"grey-text\"><time datetime=\"{$post->getDate()->format(DATE_ISO8601)}\">
            {$post->getDate()->format(DATE_RFC7231)}</time></span>";
        $html .= ' • ';
        $html .= "<span title=\"{$word_count}\" class=\"grey-text\">" . Utils::ReadTime($post->getBody()) . '</span>';
        $like_count = $post->getLikeCount();
        $like_count = sprintf($like_count === 1 ? L::posts_heart_count_singular : L::posts_heart_count_plural, $like_count);
        $html .= ' • ';
        $html .= "<span class=\"grey-text\">{$like_count}</span>";
        $html .= '</p>';
        $html .= "<h3 class=\"mt-0\"><a class=\"black-text\" href=\"{$post->getUrl()}\">{$post->getTitle()}</a></h3>";
        $html .= '</div></div>';

        return $html;
    }

    public function renderPaging()
    {
        $url = $this->category->getPostsListUrl();

        $str = '<div class="row"><div class="col s12 center">';
        $str .= '<ul class="pagination">';
        if ($this->hasPrev()) {
            $prev = $this->current_page - 1;
            $str .= "<li class=\"waves-effect\"><a href=\"{$url}?page=1\"><i class=\"fas fa-angle-double-left\"></i></a></li>";
            $str .= "<li class=\"waves-effect\"><a href=\"{$url}?page={$prev}\"><i class=\"fas fa-angle-left\"></i></a></li>";
            $str .= "<li class=\"waves-effect\"><a href=\"{$url}?page={$prev}\">{$prev}</a></li>";
        } else {
            $str .= '<li class="disabled"><a><i class="fas fa-angle-double-left"></i></a></li>';
            $str .= '<li class="disabled"><a><i class="fas fa-angle-left"></i></a></li>';
        }

        $str .= "<li class=\"waves-effect active light-blue\"><a href=\"{$url}?page={$this->current_page}\">{$this->current_page}</a></li>";

        if ($this->hasNext()) {
            $next = $this->current_page + 1;
            $str .= "<li class=\"waves-effect\"><a href=\"{$url}?page={$next}\">{$next}</a></li>";
            $str .= "<li class=\"waves-effect\"><a href=\"{$url}?page={$next}\"><i class=\"fas fa-angle-right\"></i></a></li>";
            $str .= "<li class=\"waves-effect\"><a href=\"{$url}?page={$this->max_page}\"><i class=\"fas fa-angle-double-right\"></i></a></li>";
        } else {
            $str .= '<li class="disabled"><a><i class="fas fa-angle-right"></i></a></li>';
            $str .= '<li class="disabled"><a><i class="fas fa-angle-double-right"></i></a></li>';
        }
        $str .= '</ul>';
        $str .= '</div></div>';

        return $str;
    }

    /** @return bool */
    public function hasPrev(): bool
    {
        return $this->has_prev;
    }

    /** @return bool */
    public function hasNext(): bool
    {
        return $this->has_next;
    }

    /** @return int */
    public function getTotalPosts(): int
    {
        return $this->total_posts;
    }

    /** @param int $total_posts */
    public function setTotalPosts(int $total_posts): void
    {
        $this->total_posts = $total_posts;
    }

    public function renderMeta()
    {
        $url = $this->category->getPostsListUrl();
        $meta = '';

        if ($this->current_page === 1)
            $meta .= "<link rel=\"canonical\" href=\"{$url}\" />";
        else
            $meta .= "<link rel=\"canonical\" href=\"{$url}?page={$this->current_page}\" />";

        if ($this->hasPrev()) {
            $prev = $this->current_page - 1;
            $meta .= "<link rel=\"prev\" href=\"{$url}?page={$prev}\" />";
        }

        if ($this->hasNext()) {
            $next = $this->current_page + 1;
            $meta .= "<link rel=\"next\" href=\"{$url}?page={$next}\" />";
        }

        return $meta;
    }

    public function renderJsonLd()
    {
        $url = URL::FromRoot($this->getCategory()->getPostsListUrl() . '?page=' . $this->getCurrentPage());
        $count = $this->getTotalPosts();
        $items = join(',', array_map(function ($post) {
            /** @var Post $post */
            return $post->getJsonLd();
        }, $this->getPosts()));

        $json = /** @lang JSON */ <<<JSONLD
{
    "@context": "http://schema.org",
    "@type": "ItemList",
    "url": "{$url}",
    "numberOfItems": "{$count}",
    "itemListElement": [
        {$items}
    ]
}
JSONLD;

        return "<script type=\"application/json+ld\">{$json}</script>";
    }
}
