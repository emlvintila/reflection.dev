<?php

namespace App\ViewModel;

use Framework\Model\Entity;
use InvalidArgumentException;

class CrudViewModel extends AdminViewModel
{
    public const ACTION_CREATE = 'CREATE';
    public const ACTION_READ = 'READ';
    public const ACTION_UPDATE = 'UPDATE';
    public const ACTION_DELETE = 'DELETE';
    public const ACTION_NONE = '';
    protected const ALLOWED_ACTIONS = [self::ACTION_CREATE, self::ACTION_READ, self::ACTION_UPDATE, self::ACTION_DELETE,
        self::ACTION_NONE];

    /** @var Entity */
    protected $entity;
    /** @var string */
    protected $crud_action;

    /**
     * CrudViewModel constructor.
     * @param string $crud_action
     */
    public function __construct(string $crud_action)
    {
        parent::__construct();
        $this->setCrudAction(strtoupper($crud_action));
    }

    /** @return Entity */
    public function getEntity(): ?Entity
    {
        return $this->entity;
    }

    /** @param Entity $entity */
    public function setEntity(Entity $entity): void
    {
        $this->entity = $entity;
    }

    /** @return bool */
    public function isCreateOrUpdate(): bool
    {
        $action = $this->getCrudAction();

        return $action === static::ACTION_CREATE ||
            $action === static::ACTION_UPDATE;
    }

    /** @return string */
    public function getCrudAction(): string
    {
        return $this->crud_action;
    }

    /** @param string $crud_action */
    public function setCrudAction(string $crud_action): void
    {
        if (false === array_search($crud_action, self::ALLOWED_ACTIONS))
            throw new InvalidArgumentException();

        $this->crud_action = $crud_action;
    }
}
