<?php

namespace App\ViewModel;

use App\Model\Author;
use App\Model\Post;
use Framework\ViewModel\ViewModel;
use L;

class AuthorViewModel extends ViewModel
{
    public const MAX_POSTS = 5;
    /** @var Author */
    protected $author;
    /** @var Post[] */
    protected $best_posts;
    /** @var Post[] */
    protected $recent_posts;

    /** @return Post[] */
    public function getBestPosts(): array
    {
        return $this->best_posts;
    }

    /** @param Post[] $best_posts */
    public function setBestPosts(array $best_posts): void
    {
        $this->best_posts = array_chunk($best_posts, static::MAX_POSTS)[0];
    }

    /** @return Post[] */
    public function getRecentPosts(): array
    {
        return $this->recent_posts;
    }

    /** @param Post[] $recent_posts */
    public function setRecentPosts(array $recent_posts): void
    {
        $this->recent_posts = array_chunk($recent_posts, static::MAX_POSTS)[0];
    }

    public function renderAuthorLinks(string $extra_classes = '')
    {
        return static::GetAuthorLinks($this->author, $extra_classes);
    }

    /**
     * @param Author $author
     * @param string $extra_classes
     * @return string
     */
    public static function GetAuthorLinks(Author $author, string $extra_classes = ''): string
    {
        $html = '';

        if ($ig = $author->getInstagramUsername()) {
            $title = sprintf(L::authors_social_instagram, $ig);
            $html .= <<<HTML
<a title="{$title}" data-social-media="instagram" class="author-social-link black-text" href="https://instagram.com/$ig" target="_blank">
    <i class="fab fa-instagram {$extra_classes}"></i>
</a>
HTML;
        }

        if ($fb = $author->getFacebookUsername()) {
            $title = sprintf(L::authors_social_facebook, $fb);
            $html .= <<<HTML
<a title="{$title}" data-social-media="facebook" class="author-social-link black-text" href="https://facebook.com/$fb" target="_blank">
    <i class="fab fa-facebook {$extra_classes}"></i>
</a>
HTML;
        }

        if ($gh = $author->getGithubUsername()) {
            $title = sprintf(L::authors_social_github, $gh);
            $html .= <<<HTML
<a title="{$title}" data-social-media="github" class="author-social-link black-text" href="https://github.com/$gh" target="_blank">
    <i class="fab fa-github {$extra_classes}"></i>
</a>
HTML;
        }

        if ($tw = $author->getTwitterUsername()) {
            $title = sprintf(L::authors_social_twitter, $tw);
            $html .= <<<HTML
<a title="{$title}" data-social-media="twitter" class="author-social-link black-text" href="https://twitter.com/@$tw" target="_blank">
    <i class="fab fa-twitter {$extra_classes}"></i>
</a>
HTML;
        }

        $html .= <<<'HTML'
<script type="text/javascript">
    document.querySelectorAll('.author-social-link').forEach((link) =>
        link.addEventListener('click', () =>
            gtag('event', 'view', {
                'event_category': 'author_social',
                'event_label': link.getAttribute('data-social-media')
            })
        )
    );
</script>
HTML;

        return $html;
    }

    /** @return string */
    public function renderJsonLd()
    {
        return '<script type="application/json+ld">' . $this->getAuthor()->getJsonLd() . '</script>';
    }

    /** @return Author */
    public function getAuthor(): Author
    {
        return $this->author;
    }

    /** @param Author $author */
    public function setAuthor(Author $author): void
    {
        $this->author = $author;
    }
}
