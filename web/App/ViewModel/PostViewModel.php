<?php

namespace App\ViewModel;

use App\Model\Post;
use App\OAuth\FacebookHelper;
use App\Parsedown\Parsedown;
use App\Utils;
use Framework\URL;
use Framework\ViewModel\ViewModel;
use L;

class PostViewModel extends ViewModel
{
    /** @var Post */
    protected $post;
    /** @var bool */
    protected $liked;
    /** @var Post[] */
    protected $similar_posts;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /** @return bool */
    public function isLiked(): bool
    {
        return $this->liked;
    }

    /** @param bool $liked */
    public function setLiked(bool $liked): void
    {
        $this->liked = $liked;
    }

    /** @return Post[] */
    public function getSimilarPosts(): array
    {
        return $this->similar_posts;
    }

    /** @param Post[] $similar_posts */
    public function setSimilarPosts(array $similar_posts): void
    {
        $this->similar_posts = $similar_posts;
    }

    public function renderPost()
    {
        $post = $this->post;
        $author = $post->getAuthor();
        $category = $post->getCategory();
        $html = '';

        $html .= "<article>";
        $html .= '<p class="mb-0">';

        $author_title = sprintf(L::authors_social_reflection, $author->getName());
        $html .= <<<HTML
<span>
    <a title="{$author_title}" class="black-text" href="{$author->getUrl()}">
        <i class="fas fa-user"></i>
        <span>{$author->getName()}</span>
    </a>
</span>
HTML;

        $html .= AuthorViewModel::GetAuthorLinks($author);

        $word_count = sprintf(L::posts_word_count, str_word_count($post->getBody()));
        $html .= ' • ';
        $html .= "<span class=\"grey-text\"><time datetime=\"{$post->getDate()->format(DATE_ISO8601)}\">
            {$post->getDate()->format(DATE_RFC7231)}</time></span>";
        $html .= ' • ';
        $html .= "<span title=\"{$word_count}\" class=\"grey-text\">" . Utils::ReadTime($post->getBody()) . '</span>';
        $html .= ' • ';
        $like_count = $post->getLikeCount();
        $like_count = sprintf($like_count === 1 ? L::posts_heart_count_singular : L::posts_heart_count_plural, $like_count);
        $html .= "<span class=\"grey-text\">{$like_count}</span>";
        $html .= '</p>';

        $html .= "<h2 class=\"mt-0\">{$post->getTitle()}</h2>";
        $html .= $this->renderHeartBanner();
        $html .= '<div>';
        $html .= Parsedown::instance()->parse($post->getBody());
        $html .= '</div>';
        $html .= <<<HTML
<div class="row">
    <div class="col s12">
        <p class="left">
            {$this->renderShareButtons()}
        </p>
        <p class="right">
            <a href="{$category->getUrl()}">
                <span class="new badge red ml-0 f-none d-inline-block" data-badge-caption="">
                    {$category->getFriendlyName()}
                </span>
            </a>
        </p>
    </div>
</div>
HTML;
        $html .= $this->renderHeartBanner();
        $html .= '</article>';

        return $html;
    }

    public function renderHeartBanner()
    {
        $post = $this->post;
        if ($post->getLikeCount() === 1)
            $count = sprintf(L::posts_heart_count_singular, 1);
        else
            $count = sprintf(L::posts_heart_count_plural, $post->getLikeCount());
        $liked = $this->liked ? 'heart' : 'heart-broken';

        return <<<HTML
<div class="center post-heart-divider">
    <div class="divider"></div>
    <form action="{$post->getUrl()}/like" id="like_form" method="POST">
        <button id="heart-button" class="btn-floating red accent-2" title="{$count}" type="submit"><i class="fas fa-{$liked} fa-fw fa-2x"></i>
        </button>
    </form>
</div>
HTML;
    }

    public function renderShareButtons()
    {
        $post = $this->post;
        $author = $post->getAuthor();
        $category = $post->getCategory();
        $url = URL::FromRoot($post->getUrl());
        $encoded_url = urlencode($url);
        $encoded_title = urlencode($post->getTitle());
        $fb_app_id = FacebookHelper::APP_ID;
        $html = '';

        $twitter_text = $post->getTitle();
        if ($tw = $author->getTwitterUsername()) {
            $twitter_text .= " @{$tw}";
        }
        $twitter_text .= " #{$category->getUriIdentifier()}";
        $encoded_twitter_text = urlencode($twitter_text);

        $twitter_title = L::posts_share_twitter;
        $html .= <<<HTML
<a class="share-social-link light-blue-text" data-social-media="twitter"
    title="{$twitter_title}" target="_blank" 
    href="https://twitter.com/intent/tweet?text={$encoded_twitter_text}&url={$encoded_url}">
    <i class="fab fa-twitter fa-fw fa-2x"></i>
</a>
HTML;

        $reddit_title = L::posts_share_reddit;
        $html .= <<<HTML
<a class="share-social-link light-blue-text" data-social-media="reddit"
    title="{$reddit_title}" target="_blank"
    href="https://www.reddit.com/submit?url={$encoded_url}&title={$encoded_title}">
    <i class="fab fa-reddit fa-fw fa-2x"></i>
</a>
HTML;

        $facebook_title = L::posts_share_facebook;
        $html .= <<<HTML
<a class="share-social-link light-blue-text" data-social-media="facebook"
    title="{$facebook_title}" target="_blank"
    href="https://www.facebook.com/dialog/feed?app_id={$fb_app_id}&display=popup&link={$encoded_url}">
    <i class="fab fa-facebook fa-fw fa-2x"></i>
</a>
HTML;

        $linkedin_title = L::posts_share_linkedin;
        $html .= <<<HTML
<a class="share-social-link light-blue-text" data-social-media="linkedin"
    title="{$linkedin_title}" target="_blank"
    href="https://www.linkedin.com/shareArticle?mini=true&url={$encoded_url}&title={$encoded_title}&summary=&source=">
    <i class="fab fa-linkedin fa-fw fa-2x"></i>
</a>
HTML;

        $pinterest_title = L::posts_share_pinterest;
        $pinterest_image = urlencode(URL::FromRoot($category->getSplashImage()));
        $html .= <<<HTML
<a class="share-social-link light-blue-text" data-social-media="pinterest"
    title="{$pinterest_title}" target="_blank"
    href="https://pinterest.com/pin/create/button/?url={$encoded_url}&media={$pinterest_image}&description={$encoded_title}">
    <i class="fab fa-pinterest fa-fw fa-2x"></i>
</a>
HTML;

        $email_title = L::posts_share_email;
        $html .= <<<HTML
<a class="share-social-link light-blue-text" data-social-media="email"
    title="{$email_title}" href="mailto:?subject={$post->getTitle()}&body={$encoded_url}">
    <i class="fas fa-envelope fa-fw fa-2x"></i>
</a>
HTML;

        return $html;
    }

    public function renderAllSimilar()
    {
        $html = '<ul>';

        foreach ($this->similar_posts as $post) {
            $html .= <<<HTML
<li>
    <a href="{$post->getUrl()}">{$post->getTitle()}</a> 
</li>
HTML;
        }

        $html .= '</ul>';

        return $html;
    }

    /** @return string */
    public function renderJsonLd()
    {
        return '<script type="application/json+ld">' . $this->getPost()->getJsonLd() . '</script>';
    }

    /** @return Post */
    public function getPost(): Post
    {
        return $this->post;
    }
}
