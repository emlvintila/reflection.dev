<?php

use App\Utils;
use App\ViewModel\CategoryViewModel;
use Framework\View\View;
use Framework\ViewModel\ViewModel;

$view = new View('_base.php');
/** @var CategoryViewModel $vm */
$vm = View::GetViewModel();
$category = $vm->getCategory();
?>

<?php $view->beginSection('title') ?>
<?= $category->getFriendlyName() ?>
<?php $view->endSection() ?>

<?php $view->beginSection('logo-cont') ?>
<a class="red-text" href="<?= $category->getUrl() ?>">/<?= $category->getFriendlyName() ?>
    <img class="responsive-img" src="<?= $category->getIconImage() ?>" alt=""/>
</a>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<section class="section">
    <div class="row">
        <div class="col s12 m5 push-m7">
            <div class="card">
                <a href="<?= $category->getPostsListUrl() ?>">
                    <div class="card-image">
                        <img class="responsive-img" src="<?= $category->getSplashImage() ?>"
                             alt="<?= $category->getFriendlyName() ?>"/>
                        <h4 class="card-title red center w-100 m-0"><?= $category->getFriendlyName() ?></h4>
                    </div>
                </a>
            </div>
        </div>
        <div class="col s12 m7 pull-m5">
            <h5><?= L::categories_subscription_title ?></h5>
            <p><?= sprintf(L::categories_subscription_description, $category->getFriendlyName()) ?></p>
            <?= CategoryViewModel::RenderSubscribeForm($category) ?>
            <div class="clearfix"></div>
            <?= Utils::RenderErrors(ViewModel::GetErrors()) ?>
            <?= Utils::RenderSuccess(ViewModel::GetSuccess()) ?>
        </div>

    </div>
</section>
<?php $view->endSection() ?>
