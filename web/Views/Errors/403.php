<?php
use Framework\View\View;

$view = new View('Errors/_base.php');
?>

<?php $view->beginSection('main') ?>
<h2>403</h2>
<h4><?= L::errors_pages_403 ?></h4>
<?php $view->endSection() ?>
