<?php
use Framework\App;
use Framework\View\View;

$view = new View();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error</title>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-56329527-2');
    </script>
    <?php if (App::isDevelopment()): ?>
        <link media="all" rel="stylesheet" href="/css/materialize.css"/>
        <!--<link rel="stylesheet" href="/css/font-awesome-all.css"/>-->
    <?php else: ?>
        <link media="all" rel="stylesheet" crossorigin="anonymous"
              href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
              integrity="sha256-OweaP/Ic6rsV+lysfyS4h+LM6sRwuO3euTYfr6M124g="/>
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css"
              integrity="sha256-39jKbsb/ty7s7+4WzbtELS4vq9udJ+MDjGTD5mtxHZ0=" crossorigin="anonymous"/>-->
    <?php endif ?>
    <link media="all" rel="stylesheet" href="/css/error.css" />
</head>
<body>
<main>
    <div class="section">
        <?= $view->defineSection('main', true) ?>
        <a href="/" class="btn light-blue"><?= L::errors_pages_goto_homepage ?></a>
    </div>
</main>
<?php if (App::isDevelopment()): ?>
    <script src="/js/materialize.js"></script>
<?php else: ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"
            integrity="sha256-U/cHDMTIHCeMcvehBv1xQ052bPSbJtbuiw4QA9cTKz0=" crossorigin="anonymous"></script>
    <script src="https://www.googletagmanager.com/gtag/js?id=UA-56329527-2"></script>
    <script type="text/javascript">
        gtag('event', 'exception', {
            'description': '<?= http_response_code() ?>',
            'fatal': true
        });
    </script>
<?php endif ?>
</body>
</html>
