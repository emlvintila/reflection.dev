<?php
use Framework\View\View;

$view = new View('Errors/_base.php');
?>

<?php $view->beginSection('main') ?>
<h2>404</h2>
<h4><?= L::errors_pages_404 ?></h4>
<form method="GET" action="/search">
    <div class="input-field">
        <label for="search"><?= L::search_input_placeholder ?></label>
        <input type="search" name="s" id="search"/>
    </div>
</form>
<?php $view->endSection() ?>
