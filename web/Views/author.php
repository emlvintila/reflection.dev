<?php

use App\ViewModel\AuthorViewModel;
use App\ViewModel\PostListViewModel;
use Framework\URL;
use Framework\View\View;

$view = new View('_base.php');
/** @var AuthorViewModel $vm */
$vm = View::GetViewModel();
$author = $vm->getAuthor();
$title = sprintf(L::authors_title, $author->getName());
?>

<?php $view->beginSection('title') ?>
<?= $title ?>
<?php $view->endSection() ?>

<?php $view->beginSection('meta') ?>
<meta property="og:title" content="<?= $author->getName() ?>"/>
<meta property="og:type" content="profile"/>
<meta property="og:url" content="<?= URL::FromRoot($author->getUrl()) ?>"/>
<!--<meta property="og:image" content="" />-->
<meta property="og:description" content="<?= $title ?>"/>

<meta property="profile:first_name" content="<?= $author->getName() ?>"/>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<div class="row">
    <div class="col s12">
        <h4><?= sprintf(L::authors_title, $author->getName()) ?></h4>
        <?= $vm->renderAuthorLinks('fa-2x') ?>
        <div class="row">
            <div class="col s12 m6 push-m6">
                <h4><?= L::authors_posts_recent ?></h4>
                <?php if ($recent_posts = $vm->getRecentPosts()): ?>
                    <?php foreach ($recent_posts as $post): ?>
                        <?= PostListViewModel::RenderPost($post) ?>
                    <?php endforeach ?>
                <?php else: ?>
                    <p><?= L::authors_posts_none ?></p>
                <?php endif ?>
            </div>
            <div class="col s12 m6 pull-m6">
                <h4><?= L::authors_posts_best ?></h4>
                <?php if ($best_posts = $vm->getBestPosts()): ?>
                    <?php foreach ($best_posts as $post): ?>
                        <?= PostListViewModel::RenderPost($post) ?>
                    <?php endforeach ?>
                <?php else: ?>
                    <p><?= L::authors_posts_none ?></p>
                <?php endif ?>
            </div>
        </div>

    </div>
</div>
<?php $view->endSection() ?>

<?php $view->beginSection('scripts') ?>
<?= $vm->renderJsonLd() ?>
<?php $view->endSection() ?>
