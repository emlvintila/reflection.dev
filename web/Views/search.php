<?php

use App\ViewModel\SearchViewModel;
use Framework\View\View;

$view = new View('_base.php');
/** @var SearchViewModel $vm */
$vm = View::GetViewModel();
?>

<?php $view->beginSection('title') ?>
<?= L::nav_search ?>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<section class="section">
    <div class="row">
        <div class="col s12">
            <form method="GET" action="/search">
                <div class="input-field">
                    <input required type="text" name="s" id="search" value="<?= $vm->getSearchedText() ?>"/>
                    <label for="search"><?= L::search_input_placeholder ?></label>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <?php if ($vm->hasQuery()): ?>
                <div class="row">
                    <div class="col s12">
                        <?php if ($vm->hasResults()): ?>
                            <h4><?= L::search_results_title ?></h4>
                            <?= $vm->renderAll() ?>
                        <?php else: ?>
                            <h4><?= L::search_no_results ?></h4>
                        <?php endif ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</section>
<?php $view->endSection() ?>

<?php $view->beginSection('scripts') ?>
<?php if ($vm->hasQuery()): ?>
    <script type="text/javascript">
        gtag('event', 'search', {
            'search_term': '<?= addslashes($vm->getSearchedText()) ?>'
        });
    </script>
<?php endif ?>
<?php $view->endSection() ?>
