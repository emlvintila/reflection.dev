<?php

use App\OAuthSession;
use App\Utils;
use Framework\App;
use Framework\View\View;

$view = new View();
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#" lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <?= $view->defineSection('meta') ?>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png"/>
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
    <link rel="manifest" href="/site.webmanifest"/>
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#03a9f4"/>
    <meta name="apple-mobile-web-app-title" content="reflection.dev"/>
    <meta name="application-name" content="reflection.dev"/>
    <meta name="msapplication-TileColor" content="#03a9f4"/>
    <meta name="theme-color" content="#03a9f4"/>
    <title><?= $view->defineSection('title') ?></title>

    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-56329527-2');
        <?= OAuthSession::GetInstance()->checkAndSendOAuthGtagLogin() ?>
        <?= OAuthSession::GetInstance()->checkAndSendAuthorGtagLogin() ?>
        <?= OAuthSession::GetInstance()->checkAndSendGtagUserId() ?>
    </script>
    <?php if (App::isDevelopment()): ?>
        <link media="all" rel="stylesheet" href="/css/materialize.css"/>
        <link media="all" rel="stylesheet" href="/css/highlight.js.min.css"/>
        <link media="all" rel="stylesheet" href="/css/font-awesome-all.css"/>
    <?php else: ?>
        <link media="all" rel="stylesheet" crossorigin="anonymous"
              href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
              integrity="sha256-OweaP/Ic6rsV+lysfyS4h+LM6sRwuO3euTYfr6M124g="/>
        <link media="all" rel="stylesheet" crossorigin="anonymous"
              href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/styles/default.min.css"
              integrity="sha256-zcunqSn1llgADaIPFyzrQ8USIjX2VpuxHzUwYisOwo8="/>
        <link media="all" rel="stylesheet" crossorigin="anonymous"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.css"
              integrity="sha256-39jKbsb/ty7s7+4WzbtELS4vq9udJ+MDjGTD5mtxHZ0="/>
    <?php endif ?>
    <link media="all" rel="stylesheet" href="/css/style.css"/>
    <?= $view->defineSection('stylesheets') ?>
</head>
<body>
<header>
    <?php $extended = $view->defineSection('nav-extended') ?>
    <nav class="light-blue <?= $extended ? 'nav-extended' : '' ?>">
        <div class="nav-wrapper container">
            <span class="brand-logo">
                <a href="/"><img src="/android-chrome-512x512.png"
                                 alt="Logo"/>Reflection</a><?= $view->defineSection('logo-cont') ?>
            </span>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="fas fa-bars"></i></a>
            <ul class="right hide-on-med-and-down">
                <?= Utils::RenderNavLinks() ?>
            </ul>
        </div>
        <?php if ($extended): ?>
            <div class="nav-content container clear-both">
                <?= $extended ?>
            </div>
        <?php endif ?>
    </nav>

    <ul class="light-blue sidenav" id="nav-mobile">
        <?= Utils::RenderNavLinks() ?>
        <?php if ($dashboard_links = Utils::RenderDashboardNavLinks()): ?>
            <div class="divider"></div>
            <?= $dashboard_links ?>
        <?php endif ?>
    </ul>
</header>
<main class="container">
    <?= $view->defineSection('main', true) ?>
</main>
<footer class="light-blue page-footer">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <?= $view->defineSection('footer') ?>
                <ul class="right">
                    <li><a class="white-text" href="/privacy">Privacy Policy</a></li>
                    <li><a class="white-text" href="/terms">Terms of Use</a></li>
                    <li><a class="white-text" href="/author-login">Author Login</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row mb-0">
                <div class="col s12 center">
                    <p><?= L::footer_copyright ?></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php if (App::isDevelopment()): ?>
    <script src="/js/materialize.js"></script>
    <script src="/js/timeago.js"></script>
    <script src="/js/highlight.min.js"></script>
<?php else: ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"
            integrity="sha256-U/cHDMTIHCeMcvehBv1xQ052bPSbJtbuiw4QA9cTKz0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/timeago.js/3.0.2/timeago.min.js"
            integrity="sha256-jwCP0NAdCBloaIWTWHmW4i3snUNMHUNO+jr9rYd2iOI=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.15.6/highlight.min.js"
            integrity="sha256-aYTdUrn6Ow1DDgh5JTc3aDGnnju48y/1c8s1dgkYPQ8=" crossorigin="anonymous"></script>
    <script src="https://www.googletagmanager.com/gtag/js?id=UA-56329527-2"></script>
<?php endif ?>
<script src="/js/main.js"></script>
<?= $view->defineSection('scripts') ?>
<script type="text/javascript">
    if (window.performance) {
        let timeSincePageLoad = Math.round(performance.now());
        gtag('event', 'timing_complete', {
            'name': 'load',
            'value': timeSincePageLoad
        });
    }
</script>
</body>
</html>
