<?php
use App\ViewModel\PostListViewModel;
use Framework\View\View;

$view = new View('_base.php');
/** @var PostListViewModel $vm */
$vm = View::GetViewModel();
$category = $vm->getCategory();
$title = sprintf(L::categories_posts_in, $category->getFriendlyName());
?>

<?php $view->beginSection('title') ?>
<?= $title ?>
<?php $view->endSection() ?>

<?php $view->beginSection('meta') ?>
<?= $vm->renderMeta() ?>
<?php $view->endSection() ?>

<?php $view->beginSection('logo-cont') ?>
<a class="red-text" href="<?= $category->getUrl() ?>">/<?= $category->getFriendlyName() ?></a>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<section class="section">
    <?= $vm->renderPaging() ?>
    <?= $vm->renderAll() ?>
    <?= $vm->renderPaging() ?>
</section>
<?php $view->endSection() ?>

<?php $view->beginSection('scripts') ?>
<?= $vm->renderJsonLd() ?>
<?php $view->endSection() ?>
