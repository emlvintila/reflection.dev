<?php
use App\ViewModel\CategoryListViewModel;
use Framework\View\View;

$view = new View('_base.php');
/** @var CategoryListViewModel $vm */
$vm = View::GetViewModel();
const GRID_COLUMNS = 12;
/** must be divisible by GRID_COLUMNS */
const CHUNK_SIZE = 2;
$categories_chunks = array_chunk($vm->getCategories(), CHUNK_SIZE)
?>

<?php $view->beginSection('title') ?>
<?= L::categories_title ?>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<section class="section">
    <?php foreach ($categories_chunks as $categories): ?>
        <div class="row">
            <?php foreach ($categories as $category): ?>
                <div class="col s12 m<?= GRID_COLUMNS / CHUNK_SIZE ?>">
                    <div class="card">
                        <a href="<?= $category->getPostsListUrl() ?>">
                            <div class="card-image">
                                <img class="responsive-img" src="<?= $category->getSplashImage() ?>"
                                     alt="<?= $category->getFriendlyName() ?>"/>
                                <h4 class="card-title red center w-100 m-0"><?= $category->getFriendlyName() ?></h4>
                            </div>
                        </a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    <?php endforeach ?>
</section>
<?php $view->endSection() ?>
