<?php

use App\Model\Post;
use App\Utils;
use App\ViewModel\PostCrudViewModel;
use Framework\Form\FormHelper;
use Framework\View\View;
use Framework\ViewModel\ViewModel;

$view = new View('dashboard/_base.php');
/** @var PostCrudViewModel $vm */
$vm = View::GetViewModel();
/** @var Post $post */
$post = $vm->getEntity();
$readonly = $vm->getCrudAction() === $vm::ACTION_READ || $vm->getCrudAction() === $vm::ACTION_DELETE;
$input_attributes = $readonly ? ['disabled'] : [];
$container_start = '<div class="input-field col s12">';
$container_end = '</div>';
?>

<?php $view->beginSection('main') ?>
<div class="row">
    <div class="col s12 m10 offset-m1">
        <?= Utils::RenderErrors(ViewModel::GetErrors()) ?>
        <?php switch ($vm->getCrudAction()):
            case $vm::ACTION_CREATE:
            case $vm::ACTION_READ:
            case $vm::ACTION_UPDATE:
            case $vm::ACTION_DELETE;
                ?>
                <form method="POST">
                    <?= $vm->getCrudAction() === $vm::ACTION_CREATE ? '' : FormHelper::RenderInputs(FormHelper::GetInputs($post, ['id'], ['disabled']), $container_start) ?>
                    <?= FormHelper::RenderInputs(FormHelper::GetInputs($post, ['title'], $input_attributes), $container_start) ?>
                    <?= !$readonly ? '' : FormHelper::RenderInputs(FormHelper::GetInputs($post, ['date'], $input_attributes), $container_start) ?>
                    <?= $container_start ?>
                    <select <?= !$readonly ? '' : 'disabled' ?> name="Post[category_id]" id="Post[category_id]">
                        <?php if ($readonly): ?>
                            <option value="<?= $post->getCategory()->getId() ?>"
                                    selected><?= $post->getCategory()->getFriendlyName() ?></option>
                        <?php else: ?>
                            <?php foreach ($vm->getCategories() as $category): ?>
                                <option <?= $post->getCategoryId() === $category->getId() ? 'selected' : '' ?>
                                        value="<?= $category->getId() ?>"><?= $category->getFriendlyName() ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                    <label for="Post[category_id]"><?= L::dashboard_posts_category ?></label>
                    <?= $container_end ?>
                    <?= $container_start ?>
                    <!--<label for="Post[body]">Body</label>-->
                    <!--suppress HtmlFormInputWithoutLabel -->
                    <textarea name="Post[body]" id="Post[body]"
                            <?= !$readonly ? '' : 'disabled' ?>><?= $post->getBody() ?></textarea>
                    <?= $container_end ?>

                    <?= FormHelper::RenderCsrfTag() ?>
                    <div class="col s12">
                        <?=
                        [$vm::ACTION_CREATE => '<button type="submit" class="btn light-blue waves-effect">' . L::dashboard_posts_crud_create . ' <i class="fas fa-paper-plane"></i></button>',
                            $vm::ACTION_READ => '',
                            $vm::ACTION_UPDATE => '<button type="submit" class="btn orange waves-effect">' . L::dashboard_posts_crud_update . ' <i class="fas fa-save"></i></button>',
                            $vm::ACTION_DELETE => '<button type="submit" class="btn red waves-effect">' . L::dashboard_posts_crud_delete . ' <i class="fas fa-trash"></i></button>'
                        ][$vm->getCrudAction()];
                        ?>
                    </div>
                </form>
                <?php break;
        endswitch
        ?>
    </div>
</div>
<?php $view->endSection() ?>

<?php $view->beginSection('scripts') ?>
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        const select_elements = document.querySelectorAll('select');
        /*const instances = */
        M.FormSelect.init(select_elements, {});
    });
</script>
<!--suppress JSUnresolvedFunction -->
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function () {
        const simplemde = new SimpleMDE({element: document.getElementById("Post[body]")});
        <?php if ($readonly): ?>
        simplemde.codemirror.options.readOnly = "nocursor";
        <?php endif ?>
    });
</script>
<?php $view->endSection() ?>
