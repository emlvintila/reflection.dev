<?php 
use App\Utils;
use Framework\App;
use Framework\View\View;

$view = new View('_base.php');
?>

<?php $view->beginSection('stylesheets') ?>
<?php if (App::isDevelopment()): ?>
    <link media="all" rel="stylesheet" href="/css/simplemde.min.css"/>
<?php else: ?>
    <link media="all" rel="stylesheet" crossorigin="anonymous"
          href="https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.11.2/simplemde.min.css"
          integrity="sha256-Is0XNfNX8KF/70J2nv8Qe6BWyiXrtFxKfJBHoDgNAEM="/>
<?php endif ?>
<?= $view->defineSection('stylesheets') ?>
<?php $view->endSection() ?>

<?php $view->beginSection('nav-extended') ?>
<nav class="z-depth-0 light-blue">
    <div class="nav-wrapper">
        <ul class="right hide-on-med-and-down">
            <?= Utils::RenderDashboardNavLinks() ?>
        </ul>
    </div>
</nav>
<?php $view->endSection() ?>

<?php $view->beginSection('scripts') ?>
<?php if (App::isDevelopment()): ?>
    <script src="/js/simplemde.min.js"></script>
<?php else: ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/simplemde/1.11.2/simplemde.min.js"
            integrity="sha256-6sZs7OGP0Uzcl7UDsLaNsy1K0KTZx1+6yEVrRJMn2IM=" crossorigin="anonymous"></script>
<?php endif ?>
<?= $view->defineSection('scripts') ?>
<?php $view->endSection() ?>
