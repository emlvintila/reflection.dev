<?php

use App\ViewModel\LoginViewModel;
use Framework\View\View;

$view = new View('_base.php');
/** @var LoginViewModel $vm */
$vm = View::GetViewModel();
?>

<?php $view->beginSection('title') ?>
<?= L::login_login ?>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<div class="row">
    <div class="col s12 m6 offset-m3">
        <h3 class="center"><?= L::login_login ?></h3>
        <?php if ($fb = $vm->getFacebookLoginUrl()): ?>
            <div class="row">
                <div class="center col s12">
                    <a class="btn w-100 waves-effect blue darken-2" href="<?= $fb ?>"
                       title="<?= L::login_facebook ?>" id="facebook-login-button">
                        <?= L::login_facebook ?> <i class="fab fa-facebook"></i></a>
                </div>
            </div>
        <?php endif ?>
        <?php if ($tw = $vm->getTwitterLoginUrl()): ?>
            <div class="row">
                <div class="center col s12">
                    <a class="btn w-100 waves-effect blue lighten-1" href="<?= $tw ?>"
                       title="<?= L::login_twitter ?>" id="twitter-login-button">
                        <?= L::login_twitter ?> <i class="fab fa-twitter"></i></a>
                </div>
            </div>
        <?php endif ?>
        <?php if ($go = $vm->getGoogleLoginUrl()): ?>
            <div class="row">
                <div class="center col s12">
                    <a class="btn w-100 waves-effect red lighten-1" href="<?= $go ?>"
                       title="<?= L::login_google ?>" id="google-login-button">
                        <?= L::login_google ?> <i class="fab fa-google"></i></a>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>
<?php $view->endSection() ?>

<?php $view->beginSection('scripts') ?>
<script type="text/javascript">
    document.querySelector('#facebook-login-button').addEventListener('click',
        () => gtag('event', 'login', {method: 'Facebook'}));
    document.querySelector('#twitter-login-button').addEventListener('click',
        () => gtag('event', 'login', {method: 'Twitter'}));
    document.querySelector('#google-login-button').addEventListener('click',
        () => gtag('event', 'login', {method: 'Google'}));
</script>
<?php $view->endSection() ?>
