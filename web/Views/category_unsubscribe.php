<?php

use App\Utils;
use App\ViewModel\CategoryViewModel;
use Framework\View\View;
use Framework\ViewModel\ViewModel;

$view = new View('_base.php');
/** @var CategoryViewModel $vm */
$vm = View::GetViewModel();
$category = $vm->getCategory();
?>

<?php $view->beginSection('title') ?>
<?= $category->getFriendlyName() ?>
<?php $view->endSection() ?>

<?php $view->beginSection('logo-cont') ?>
<a class="red-text" href="<?= $category->getUrl() ?>">/<?= $category->getFriendlyName() ?>
    <img class="responsive-img" src="<?= $category->getIconImage() ?>" alt=""/>
</a>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<section class="section">
    <div class="row">
        <div class="col s12 m6 offset-m3">
            <h5><?= L::categories_subscription_inputs_unsubscribe_submit ?></h5>
            <?= CategoryViewModel::RenderUnsubscribeForm($category) ?>
            <div class="clearfix"></div>
            <?= Utils::RenderErrors(ViewModel::GetErrors()) ?>
            <?= Utils::RenderSuccess(ViewModel::GetSuccess()) ?>
        </div>
    </div>
</section>
<?php $view->endSection() ?>
