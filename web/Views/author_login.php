<?php

use App\Utils;
use Framework\Form\FormHelper;
use Framework\View\View;
use Framework\ViewModel\ViewModel;

$view = new View('_base.php');
$vm = View::GetViewModel();
?>

<?php $view->beginSection('title') ?>
<?= L::author_login_title ?>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<div class="row">
    <div class="col s12 m6 offset-m3 center">
        <h4><?= L::author_login_title ?></h4>
        <form method="POST">
            <div class="input-field col s12">
                <label for="email"><?= L::author_login_email ?></label>
                <input type="email" name="email" id="email"/>
            </div>
            <div class="input-field col s12">
                <label for="password"><?= L::author_login_password ?></label>
                <input type="password" name="password" id="password"/>
            </div>

            <?= FormHelper::RenderCsrfTag() ?>
            <button id="author-login-button" class="btn light-blue waves-effect"
                    type="submit">
                <?= L::author_login_submit ?> <i class="fas fa-lock-open"></i>
            </button>
        </form>
        <?= Utils::RenderErrors(ViewModel::GetErrors()) ?>
    </div>
</div>
<?php $view->endSection() ?>
