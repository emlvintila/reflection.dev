<?php

use App\ViewModel\CategoryViewModel;
use App\ViewModel\PostViewModel;
use Framework\Session\Session;
use Framework\URL;
use Framework\View\View;

$view = new View('_base.php');
/** @var PostViewModel $vm */
$vm = View::GetViewModel();
$post = $vm->getPost();
$category = $post->getCategory();
$author = $post->getAuthor();
$author_is_author = Session::GetInstance()->getUserId() === $author->getId();
$description = addslashes($post->getFirstParagraph());
?>

<?php $view->beginSection('meta') ?>
<link rel="canonical" href="<?= $post->getUrl() ?>"/>
<meta property="og:type" content="article"/>
<meta property="og:title" content="<?= addslashes($post->getTitle()) ?>"/>
<meta property="og:url" content="<?= URL::FromRoot($post->getUrl()) ?>"/>
<meta property="og:image" content="<?= URL::FromRoot($category->getSplashImage()) ?>"/>
<meta property="og:description" content="<?= $description ?>"/>

<meta property="article:published_time" content="<?= $post->getDate()->format(DATE_ISO8601) ?>"/>
<meta property="article:section" content="<?= $category->getFriendlyName() ?>"/>
<meta property="article:author" content="<?= URL::FromRoot($author->getUrl()) ?>"/>
<meta name="description" content="<?= $description ?>"/>
<?php $view->endSection() ?>

<?php $view->beginSection('title') ?>
<?= $post->getTitle() ?>
<?php $view->endSection() ?>

<?php $view->beginSection('logo-cont') ?>
<a class="red-text" href="<?= $category->getUrl() ?>">/<?= $category->getFriendlyName() ?>
    <img class="responsive-img" src="<?= $category->getIconImage() ?>" alt=""/>
</a>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>
<?php if ($author_is_author): ?>
    <div class="fixed-action-btn">
        <a href="/dashboard/posts/update/<?= $post->getId() ?>" class="btn-floating btn-large red">
            <i class="fas fa-edit"></i>
        </a>
    </div>
<?php endif ?>
<div class="row">
    <div class="col s12">
        <section class="section">
            <?= $vm->renderPost() ?>
        </section>
    </div>
</div>
<div class="row">
    <div class="col s12 m6">
        <h5><?= L::posts_subscribe_title ?></h5>
        <p><?= sprintf(L::posts_subscribe_description, $post->getCategory()->getFriendlyName()) ?></p>
        <?= CategoryViewModel::RenderSubscribeForm($post->getCategory()) ?>
    </div>
    <div class="col s12 m6">
        <h5><?= L::posts_similar ?></h5>
        <?= $vm->renderAllSimilar() ?>
    </div>
</div>
<?php $view->endSection() ?>

<?php $view->beginSection('scripts') ?>
<?= $vm->renderJsonLd() ?>
<script type="text/javascript">
    gtag('config', 'UA-56329527-2', {
        'page_title': '<?= $category->getFriendlyName() ?>',
        'page_path':
            '<?= join('/', explode('/', $post->getUrl(), -1)) ?>'
    });
    document.querySelector('#like_form').addEventListener('submit',
        () => gtag('event', '<?= $vm->isLiked() ? 'unheart' : 'heart' ?>', {
            'event_category': 'post_heart',
            'event_label': '<?= addslashes($post->getTitle()) ?>'
        }));
    document.querySelectorAll('.share-social-link').forEach(
        (link) => link.addEventListener('click',
            () => gtag('event', 'share', {
                'method': link.getAttribute('data-social-media')
            })));
</script>
<?php $view->endSection() ?>
