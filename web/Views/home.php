<?php

use Framework\URL;
use Framework\View\View;

$view = new View('_base.php');
// TODO: Design home page
$title = 'Reflection';
?>

<?php $view->beginSection('meta') ?>
<meta property="og:title" content="<?= $title ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="<?= URL::FromRoot('') ?>"/>
<meta property="og:image" content=""/>
<meta property="og:description" content="<?= L::meta_description ?>"/>
<meta name="description" content="<?= L::meta_description ?>"/>
<?php $view->endSection() ?>

<?php $view->beginSection('title') ?>
<?= $title ?>
<?php $view->endSection() ?>

<?php $view->beginSection('main') ?>

<?php $view->endSection() ?>
