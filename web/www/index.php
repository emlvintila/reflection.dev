<?php require '../App/bootstrap.php';

use App\Controller\DefaultController;
use App\Logging\DefaultDatabaseLogger;
use App\Logging\MixedLogger;
use App\OAuthSession;
use Framework\App;
use Framework\Logging\FileLogger;
use Framework\Logging\Logger;
use Framework\Utils;

$app = App::GetInstance();
// calls the constructor
OAuthSession::GetInstance();
setcookie(session_name(), session_id(), time() + 60 * 60 * 24 * 30, '/',
    '.reflection.to', true, true);
$app->configureFrom(__DIR__ . '/../config_dev.json');

$logger = new MixedLogger(
    new DefaultDatabaseLogger($app->getDatabaseConnections()['logs']),
    new FileLogger(__DIR__ . '/../logs/default.log')
);
$error_handler = function (int $log_severity, int $http_code, ...$args) use ($logger) {
    if (App::isDevelopment()) {
        Utils::Dump($args);
    } else {
        $logger->log($log_severity, $args);
        $controller = new DefaultController();
        $controller->renderHttpStatusPage($http_code);
    }
};

register_shutdown_function(function () use ($error_handler) {
    $error = error_get_last();
    if ($error !== NULL && $error['type'] === E_ERROR) {
        $error_handler(Logger::LOG_FATAL, 500, $error);
        // TODO: Email here
    }
});
$app->registerExceptionHandler(function (Exception $exception) use ($error_handler) {
    $error_handler(Logger::LOG_ERROR, 503, [$exception->getMessage(), $exception->getTraceAsString()]);
});

$app->getDependencyInjectionService()
    ->register($app->getDatabaseConnections()['default'])
    ->register($logger);
$app->run();
