document.addEventListener('DOMContentLoaded', function () {
    // noinspection JSUnresolvedFunction
    timeago().render(document.querySelectorAll('time'));
    // noinspection JSUnresolvedVariable,ES6ModulesDependencies
    hljs.initHighlightingOnLoad();
    /*let instances = */M.Sidenav.init(document.querySelectorAll('.sidenav'), {});
});
