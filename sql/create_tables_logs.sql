CREATE SCHEMA IF NOT EXISTS reflection_logs;

CREATE TABLE reflection_logs.logs
(
    id       SERIAL                   NOT NULL
        CONSTRAINT logs_pk
            PRIMARY KEY,
    severity INT                      NOT NULL DEFAULT 1,
    text     TEXT                     NOT NULL,
    date     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp
);
