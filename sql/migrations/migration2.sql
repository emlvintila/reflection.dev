ALTER TABLE reflection.posts
    ADD COLUMN IF NOT EXISTS title_metaphone TEXT;
ALTER TABLE reflection.posts
    ALTER title_metaphone DROP NOT NULL;

CREATE OR REPLACE FUNCTION reflection.posts_title_metaphone_proc()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
    SECURITY DEFINER
AS
$BODY$
DECLARE
    arr text[];
    result text[];
    entry text;
BEGIN
    arr := regexp_split_to_array(NEW.title, '\s+');
    FOREACH entry IN ARRAY arr LOOP
        result := array_append(result, metaphone(entry, 100));
    END LOOP;
    NEW.title_metaphone = array_to_string(result, ' ');
    RETURN NEW;
END
$BODY$;

DROP TRIGGER IF EXISTS posts_title_metaphone_trigger ON reflection.posts;
CREATE TRIGGER posts_title_metaphone_trigger
    BEFORE INSERT OR UPDATE
    ON reflection.posts
    FOR EACH ROW
EXECUTE PROCEDURE reflection.posts_title_metaphone_proc();

UPDATE reflection.posts
SET id=id;

ALTER TABLE reflection.posts
    ALTER title_metaphone SET NOT NULL;
