ALTER TABLE reflection.posts
    ADD IF NOT EXISTS date_updated TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT current_timestamp;

CREATE OR REPLACE FUNCTION reflection.posts_date_updated_trigger_proc()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
    SECURITY DEFINER
AS
$BODY$
BEGIN
    NEW.date_updated = current_timestamp;
    RETURN NEW;
END
$BODY$;

DROP TRIGGER IF EXISTS posts_date_updated_trigger ON reflection.posts;
CREATE TRIGGER posts_date_updated_trigger
    BEFORE UPDATE
    ON reflection.posts
    FOR EACH ROW
EXECUTE PROCEDURE reflection.posts_date_updated_trigger_proc();

UPDATE reflection.posts
SET id=id;
