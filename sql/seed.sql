INSERT INTO reflection.categories (uri_identifier, friendly_name, splash_image)
VALUES ('php', 'PHP', '/images/php_splash.png');

INSERT INTO reflection.authors (name, email, password_hash, instagram_username, facebook_username, github_username,
                                twitter_username)
VALUES ('Emanuel', 'emanuel@reflection.to', '$2y$10$Sz5/jpfHRU/WOMwDYZ3Js.iPjoBBhIiephvDo0EMxCeMHpm1KZ4mq',
        'emanuelvintila', 'emanuel.vintila.3', 'emanuelvintila', '@emanuelvintila');

INSERT INTO reflection.posts (title, body, category_id, author_id)
VALUES ('Test Title', 'test body',
        (SELECT id FROM reflection.authors WHERE name = 'Emanuel' LIMIT 1),
        (SELECT id FROM reflection.categories WHERE uri_identifier = 'php' LIMIT 1));
