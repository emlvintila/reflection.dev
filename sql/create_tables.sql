CREATE SCHEMA IF NOT EXISTS reflection;

CREATE TABLE reflection.categories
(
    id             SERIAL NOT NULL
        CONSTRAINT languages_pk
            PRIMARY KEY,
    uri_identifier TEXT   NOT NULL,
    friendly_name  TEXT   NOT NULL,
    icon_image     TEXT   NOT NULL DEFAULT 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wUWCCcANUCnYgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAALSURBVAjXY2AAAgAABQAB4iYFmwAAAABJRU5ErkJggg==',
    splash_image   TEXT   NOT NULL DEFAULT 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAABmJLR0QAAAAAAAD5Q7t/AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wUWCCcANUCnYgAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAALSURBVAjXY2AAAgAABQAB4iYFmwAAAABJRU5ErkJggg=='
);
CREATE UNIQUE INDEX categories_uri_identifier_uindex
    ON reflection.categories (uri_identifier);


CREATE TABLE reflection.authors
(
    id                 SERIAL NOT NULL
        CONSTRAINT authors_pk
            PRIMARY KEY,
    name               TEXT   NOT NULL,
    email              TEXT   NOT NULL,
    password_hash      TEXT   NOT NULL,
    instagram_username TEXT,
    facebook_username  TEXT,
    github_username    TEXT,
    twitter_username   TEXT
);
CREATE UNIQUE INDEX authors_email_uindex
    ON reflection.authors (email);


CREATE TABLE reflection.posts
(
    id          SERIAL
        CONSTRAINT posts_pk
            PRIMARY KEY,
    title       TEXT NOT NULL,
    body        TEXT NOT NULL,
    date        TIMESTAMP WITH TIME ZONE DEFAULT current_timestamp,
    category_id INT  NOT NULL
        CONSTRAINT posts_categories_id_fk
            REFERENCES reflection.categories (id)
            ON UPDATE CASCADE ON DELETE RESTRICT,
    author_id   INT  NOT NULL
        CONSTRAINT posts_authors_id_fk
            REFERENCES reflection.authors (id)
            ON UPDATE CASCADE ON DELETE SET NULL
);


CREATE TYPE reflection.OAUTH_PROVIDER AS ENUM ('facebook', 'twitter', 'instagram', 'github');
CREATE TABLE reflection.posts_likes
(
    id       SERIAL
        CONSTRAINT posts_likes_pk
            PRIMARY KEY,
    user_id  TEXT                      NOT NULL,
    provider reflection.OAUTH_PROVIDER NOT NULL,
    date     TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT current_timestamp,
    post_id  INT                       NOT NULL
        CONSTRAINT posts_likes_posts_id_fk
            REFERENCES reflection.posts (id)
            ON UPDATE CASCADE ON DELETE SET NULL
);


CREATE TABLE reflection.mailing_list_subscriptions
(
    id          SERIAL NOT NULL
        CONSTRAINT mailing_list_subscriptions_pk
            PRIMARY KEY,
    name        TEXT,
    email       TEXT   NOT NULL,
    language_id INT    NOT NULL
        CONSTRAINT mailing_list_subscriptions_categories_id_fk
            REFERENCES reflection.categories (id)
            ON UPDATE CASCADE ON DELETE CASCADE
);

DROP EXTENSION IF EXISTS fuzzystrmatch;
CREATE EXTENSION fuzzystrmatch;
